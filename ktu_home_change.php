<?php
ob_start();
require_once "functions.php";

	//Random mail name and cookie
    function create_random_mail(){
    	$random_name = randomName();
        do{
            $result=database_compare($random_name,"ktumail.com",true);
            if(!$result)
                $random_name = randomName();
        }while(!$result);
        $db_result=save_to_db($random_name,"ktumail.com");
        if($db_result)
            die();
        setcookie("TMA",$random_name,time()+60*10);//TMA: Temporary Mail Address

        return  $random_name . "@ktumail.com";
    }
	if(!isset($_COOKIE['TMA'])){
        $tma_name = create_random_mail();
	}
	else{
		$result = database_compare(trim($_COOKIE['TMA']),"ktumail.com",false);
		$user = compare_right_user(trim($_COOKIE['TMA']),"ktumail.com", getUserIpAddr());
        if(!$result || empty(trim($_COOKIE['TMA'])) || !$user)
            $tma_name =create_random_mail();
        else
            $tma_name = $_COOKIE['TMA']."@ktumail.com";
	}
	
	$browser_lg = substr(locale_accept_from_http($_SERVER["HTTP_ACCEPT_LANGUAGE"]), 0 ,2);
	$lg = isset($_GET["lang"]) ? $_GET["lang"] : '';
	$lg = !empty($_GET["lang"]) ? $_GET["lang"] : '';

	if (!isset($_COOKIE["LANG"]) || ($_COOKIE["LANG"] != "id" && $_COOKIE["LANG"] != "de" && $_COOKIE["LANG"] != "en" && $_COOKIE["LANG"] != "es" && $_COOKIE["LANG"] != "fr" && $_COOKIE["LANG"] != "it" && $_COOKIE["LANG"] != "nl" && $_COOKIE["LANG"] != "no" && $_COOKIE["LANG"] != "pt" && $_COOKIE["LANG"] != "sv" && $_COOKIE["LANG"] != "tr" && $_COOKIE["LANG"] != "ru" && $_COOKIE["LANG"] != "uk" && $_COOKIE["LANG"] != "hi" && $_COOKIE["LANG"] != "zh-cn" && $_COOKIE["LANG"] != "ja")) {
		if ($browser_lg == "id" || $browser_lg == "de" || $browser_lg == "en" || $browser_lg == "es" || $browser_lg == "fr" || $browser_lg == "it" || $browser_lg == "nl" || $browser_lg == "no" || $browser_lg == "pt" || $browser_lg == "sv" || $browser_lg == "tr" || $browser_lg == "ru" || $browser_lg == "uk" || $browser_lg == "hi" || $browser_lg == "zh" || $browser_lg == "ja") {
			setcookie("LANG", $browser_lg, time()+60*60*24*365);
			$_COOKIE["LANG"] = $browser_lg;
		}
		else {
			setcookie("LANG", "en", time()+60*60*24*365);
			$_COOKIE["LANG"] = "en";
		}
	}

	if ($lg == "id" || $lg == "de" || $lg == "en" || $lg == "es" || $lg == "fr" || $lg == "it" || $lg == "nl" || $lg == "no" || $lg == "pt" || $lg == "sv" || $lg == "tr" || $lg == "ru" || $lg == "uk" || $lg == "hi" || $lg == "zh-cn" || $lg == "ja") {
		setcookie("LANG", $lg, time()+60*60*24*365);
		$_COOKIE["LANG"] = $lg;
	}

	$created_mail_count = number_format(how_many_mail());

	include "languages/lang.php";
	
	date_default_timezone_set('Europe/Istanbul');
?>
<!DOCTYPE html>
<html lang="<?=$_COOKIE["LANG"]?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript">
		<?php
			$start_date = getMailTime(substr($tma_name,0,5));
			$date1 = (new DateTime($start_date))->format("Y, m, d, H, i, s");
		?>
		var date1 = new Date(<?= $date1 ?>);
		var date2 = new Date(<?= date("Y, m, d, H, i, s") ?>);
	</script>
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script>var firstMail = true;</script>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" type="image/png" sizes="64x64" href="css/icons/favicon.png">
	<link rel="apple-touch-icon" sizes="180x180" href="css/icons/apple-touch-icon.png">
	<meta name="msapplication-square70x70logo" content="css/icons/ms-70.png">
	<meta name="msapplication-square150x150logo" content="css/icons/ms-150.png">
	<meta name="msapplication-square310x310logo" content="css/icons/ms-310.png">
	<meta name="msapplication-wide310x150logo" content="css/icons/ms-rect-310.png">
	<meta name="msapplication-TileColor" content="#ebeff6">
	<title><?=$lang["title_tag"]?></title>
	<meta name="description" content="<?=$lang["meta_description"]?>">
	<meta property="og:title" content="<?=$lang["title_tag"]?>">
	<meta property="og:url" content="https://10mails.net/">
	<meta property="og:description" content="<?=$lang["meta_description"]?>">
	<meta property="og:image" content="https://10mails.net/css/icons/og-300x200.png">
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:description" content="<?=$lang["meta_description"]?>"/>
	<meta name="twitter:title" content="<?=$lang["title_tag"]?>"/>
	<meta name="twitter:site" content="@10mailsnet"/>
	<meta name="twitter:creator" content="@10mailsnet"/>
	<meta name="twitter:image" content="https://10mails.net/css/icons/og-300x200.png"/>
	<link rel="canonical" href="https://10mails.net/" />
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140125866-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-140125866-1');
	</script>
	
</head>
<body>
		



	<!-- CONTAINER -->


	<?php include 'ktu_container.php'; ?>





	<!-- BODY -->


	<div class="main">

		<div class="w-headline">
			<p class="headline-h1"><?=$lang["welcome"]?></p>
			<p class="headline-p"><?=$lang["this_is_your"]?><span class="q-mark" onclick="show_modal('modal_about')">.</span></p>
		</div>

		<div class="email">

			<div class="email-box-frame">
				<span id="tma" class="email-box email-gen"><?=$tma_name?></span>
				<span class="copy-email email-gen tooltip" onclick="copy('tma')">
					<span id="copy_email" class="tooltiptext tooltip-res"><?=$lang["copy"]?></span>
				</span>
			</div>

			<div class="res-countdown-frame">
				<div id="cd_frame" class="countdown-frame">
					<span id="giveme60" class="giveme60 countdown-gen tooltip" onclick="extend_time('<?=substr($tma_name,0,5)?>','ktumail.com')">
						<span id="stop_time" class="tooltiptext tt-hg tooltip-res"><?=$lang["extend_time"]?></span>
					</span>
					<span class="countdown countdown-gen" id="scountdown">
						<span class="monospace" id="min1"></span>
						<span class="monospace" id="min0"></span>
						<span class="monospace reagent" id="reagent1">:</span>
						<span class="monospace" id="sec1"></span>
						<span class="monospace" id="sec0"></span>
						<span class="monospace reagent" id="reagent0">:</span>
						<span class="monospace" id="ssec1"></span>
						<span class="monospace" id="ssec0"></span>
					</span>
				</div>
			</div>

			<div id="signup_to" class="signup-to">
				<div>
					<div class="signup-to-h"><b><?=$lang["signup_with_this"]?></b></div>
					<div onclick="close_modal_popup(this)" class="close-icon close-small"></div>
					<a href="https://www.facebook.com/r.php" target="_blank"><span class="all-icons facebook-icon">Facebook</span></a>
					<a href="https://twitter.com/signup" target="_blank"><span class="all-icons twitter-icon">Twitter</span></a>
					<a href="https://www.instagram.com/accounts/emailsignup/" target="_blank"><span class="all-icons instagram-icon">Instagram</span></a>
					<a href="https://vk.com/join" target="_blank"><span class="all-icons vk-icon">VK</span></a>
					<!--<a href="https://www.linkedin.com/reg/join" target="_blank"><span class="all-icons linkedin-icon">Linkedin</span></a>-->
					<a href="https://www.netflix.com/signup" target="_blank"><span class="all-icons netflix-icon">Netflix</span></a>
					<?php
						if ($_COOKIE["LANG"] == "tr") {
					?>
					<a href="https://www.blutv.com.tr/kayit" target="_blank"><span class="all-icons blutv-icon">Blutv</span></a>
					<!--<a href="https://puhutv.com/kayit-ol" target="_blank"><span class="all-icons puhutv-icon">Puhutv</span></a>-->
					<?php
						}
					?>
					<a href="https://www.tumblr.com/register" target="_blank"><span class="all-icons tumblr-icon">Tumblr</span></a>
					<a href="https://www.pinterest.com/signup" target="_blank"><span class="all-icons pinterest-icon">Pinterest</span></a>
				</div>
			</div>

		</div>

		<div class="mailbox">
			
			<div class="mailbox-headline">
				<span class="mailbox-head mailbox-head-gen"><?=$lang["inbox"]?></span>
				<span class="refresh mailbox-head-gen tooltip" onclick="location.reload();">
					<span id="copy_email" class="tooltiptext tt-hg"><?=$lang["refresh"]?></span>
				</span>
			</div>
			<div id="mailbox-inner" class="mailbox-inner">
				<?php
					$n_msgs = mail_count(substr($tma_name,0,5), $mbox);
					if($n_msgs>0):
					$mails = mail_header(substr($tma_name,0,5), $mbox);
					$count = 0;
					foreach($mails as $mail):
				?>
				<div id="mail" class="mail opnd" onclick="show_modal('modal_mail'); load_content2(<?=$mail['msg_no']?>)">
					<div id="mail_from" class="mail-from opndrd"><?=imap_utf8(htmlentities($mail['from']))?></div>
					<div id="mail_content" class="mail-content"><b><?=imap_utf8(htmlentities($mail['subject']))?></b>&nbsp;&nbsp;<?=imap_utf8(htmlentities($mail['body']))?></div>
				</div>
				<?php
					$count++;
					endforeach;
				?>
				<div id="total-mail" class="total-mail"><?=$lang["total_mail"]?></div><!--php//$n_msgs-->
				<?php
					else:
				?>
				<div class="no-mail">
					<div class="sad-icon"></div>
					<div class="no-mail-text"><b><?=$lang["no_email"]?></b></div>
				</div>
				<?php endif; ?>
			</div>						
		</div>
	</div>

	<!-- SEO -->
	<div class="white-bg">
		<div class="main mt50">
			<div class="what-is-temporary-mail">
				<h1>10 dakikalık geçici e-posta nedir?</h1>
				<p class="p18 mt30">Belirli bir süre geçtikten sonra kendini imha eden e-posta adresleridir. Temp mail, 10 minute mail, disposable email, guerrilla mail, throwaway e-posta, sahte posta veya çöp kutusu gibi isimlerle de bilinir.</p>
				<p class="p18 mt20">Günümüzde internet üzerindeki birçok işlemi gerçekleştirebilmek için e-posta adresimizi vermemiz gerekir. Örneğin bir içeriğe ulaşmak, bir dosyayı indirmek ya da bir servisi kullanmak için e-posta adresimizi vermek zorunda kalırız. Bu sebeple de gelen kutumuz spam maillerle dolup taşar. 10 dakikalık geçici e-posta spam maillerden korunmanın en iyi yoludur.</p>
			</div>
			<div class="features">
				<div class="shield-icon"></div>
				<h2>Güvenli</h2>
				<p class="p18 mt12 w250">E-posta adresleri tek kullanımlıktır ve sizden başkası erişemez.</p>
			</div>
			<div class="features">
				<div class="dashboard-icon"></div>
				<h2>Hızlı</h2>
				<p class="p18 mt12 w250">E-postalar en kısa sürede gelen kutunuza düşer. Böylece beklemek zorunda kalmazsınız.</p>
			</div>
			<div class="features">
				<div class="time-2-icon"></div>
				<h2>Uzatılabilir Süre</h2>
				<p class="p18 mt12 w250">10 dakika bana yetmez diyorsanız süreyi bir saat uzatabilirsiniz.</p>
			</div>
		</div>
	<!-- FOOTER -->
	<?php include 'ktu_footer.php'; ?>
	</div>

	<script>
	$(function(){
		function mail_total_count(mail_total){
				$("#total-mail").children("b").text(mail_total);
		}
		var old_mail_count = <?= $n_msgs ?>;
		var new_mail_count = 0;
		mail_total_count(old_mail_count);
		setInterval(function(){
				$.ajax({
					type:   'post',
					url:    'functions.php',
					data:   {'mailNameC': "<?=substr($tma_name,0,5)?>"},
					success: function(response1){
							new_mail_count = response1;
							if(new_mail_count > old_mail_count){
								var mail_count = new_mail_count - old_mail_count;
								$.ajax({
									type:   'post',
									url:    'functions.php',
									data:   {'mailName': "<?=substr($tma_name,0,5)?>", 'old_count': mail_count},
									dataType: 'json',
									success: function(response){
										for(var i=(response.length-1); i>=0; i--){
											var mailItem = $('<div></div>');
											mailItem.attr('id', 'mail');
											mailItem.addClass("mail");
											mailItem.on('click',show_modal_x);
											//mailItem.on('click',{val : "modal_mail"},show_modal);
											mailItem.on('click',{val : response[i]["msg_no"]},load_content);
											$('#mailbox-inner').prepend(mailItem);
											var mailFromItem = $('<div></div>');
											mailFromItem.attr('id', 'mail_from');
											mailFromItem.addClass("mail-from");
											mailItem.append(mailFromItem);
											mailFromItem.text(response[i]["from"]);
											var mailContentItem = $('<div></div>');
											mailContentItem.attr('id', 'mail_content');
											mailContentItem.addClass("mail-content");
											mailItem.append(mailContentItem);
											var subject = $('<b></b>');
											mailContentItem.append(subject);
											subject.text(response[i]["subject"]);
											mailContentItem.append(("  "+response[i]["body"]).replace(/ /g, "&nbsp;"));
										}
										if(new_mail_count==1 && firstMail){
											$(".no-mail").remove();
											var total_mail = $('<div></div>');
											total_mail.attr('id', 'total-mail');
											total_mail.addClass("total-mail");
											$("#mailbox-inner").append(total_mail);
											total_mail.text("Toplam ");
											var b_tag = $('<b></b>');
											total_mail.append(b_tag);
											var span_tag = $('<span></span>');
											total_mail.append(span_tag);
											span_tag.text(" e-postan var.");
											firstMail = false;
										}
										old_mail_count = new_mail_count;
										mail_total_count(old_mail_count);
									}
								});
							}
					}
				});
		},10000);				
	});
	</script>

	<!-- MODAL_ABOUT -->

	<div id="modal_about" class="modal-main">
		<div class="modal-content">
			<div class="modal-header">
				<p class="modal-h1"><?=$lang["about"]?></p>
				<div onclick="close_modal('modal_about')" class="close-icon"></div>
			</div>
			<div class="modal-body">
				<h2 class="hl2 mt0"><?=$lang["whats_10mails"]?></h2>
				<p class="pr"><?=$lang["10mails_is"]?></p>
				<p class="pr"><?=$lang["10mails_is_2"]?></p>
				<h2 class="hl2"><?=$lang["where_can_i_use"]?></h2>
				<p class="pr"><?=$lang["you_can_use"]?></p>
			</div>
			<div class="modal-footer">
				<span class="modal-btn" onclick="close_modal('modal_about')"><?=$lang["got_it"]?></span>
			</div>
		</div>
	</div>

	<!-- MODAL_LANG -->

	<div id="modal_lang" class="modal-main">
		<div class="modal-content">
			<div class="modal-header">
				<p class="modal-h1"><?=$lang["languages"]?></p>
				<div onclick="close_modal('modal_lang')" class="close-icon"></div>
			</div>
			<div class="modal-body">
				<div class="lg-list">
					<a href="?lang=id"><p class="modal-p"><?=$lang["indonesian"]?></p></a>
					<a href="?lang=de"><p class="modal-p"><?=$lang["german"]?></p></a>
					<a href="?lang=en"><p class="modal-p"><?=$lang["english"]?></p></a>
					<a href="?lang=es"><p class="modal-p"><?=$lang["spanish"]?></p></a>
					<a href="?lang=fr"><p class="modal-p"><?=$lang["french"]?></p></a>
					<a href="?lang=it"><p class="modal-p"><?=$lang["italian"]?></p></a>
					<a href="?lang=nl"><p class="modal-p"><?=$lang["dutch"]?></p></a>
					<a href="?lang=no"><p class="modal-p"><?=$lang["norwegian"]?></p></a>
				</div>
				<div class="lg-list">
					<a href="?lang=pt"><p class="modal-p"><?=$lang["portuguese"]?></p></a>
					<a href="?lang=sv"><p class="modal-p"><?=$lang["swedish"]?></p></a>
					<a href="?lang=tr"><p class="modal-p"><?=$lang["turkish"]?></p></a>
					<a href="?lang=ru"><p class="modal-p"><?=$lang["russian"]?></p></a>
					<a href="?lang=uk"><p class="modal-p"><?=$lang["ukranian"]?></p></a>
					<a href="?lang=hi"><p class="modal-p"><?=$lang["hindu"]?></p></a>
					<a href="?lang=zh-cn"><p class="modal-p"><?=$lang["chinese-simplified"]?></p></a>
					<a href="?lang=ja"><p class="modal-p"><?=$lang["japanese"]?></p></a>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL_MAIL -->

	<div id="modal_mail" class="modal-main">
		<div class="modal-content">
			<div class="modal-header">
				<p id="subject" class="modal-h1"></p>
				<div onclick="close_modal('modal_mail')" class="close-icon"></div>
			</div>
			<div class="modal-body m-mail" id="body"></div>
		</div>
	</div>

	<!-- MODAL_BG -->

	<div id="modal_bg" class="modal"></div>

	<!-- POPUP_SHARE_US -->

	<div id="popup_share_us" class="popup">
		<div class="popup-inner">
				<div class="popup-header"><?=$lang["do_u_like"]?></div>
				<div onclick="close_modal_popup_effect(this)" class="close-icon popup-close"></div>
				<a href="https://api.whatsapp.com/send?text=<?=$lang["share_txt"]?>" target="_blank"><span class="all-icons whatsapp-icon">WhatsApp</span></a>
				<?php
					$OS = $_SERVER["HTTP_USER_AGENT"];
					//if (stristr($OS, "Android") || stristr($OS, "iPhone OS")) {
					if (stristr($OS, "iPhone OS")) {
				?>
				<a href="sms:+901234567890&body=<?=$lang["share_txt"]?>"><span class="all-icons sms-icon">SMS</span></a>
				<?php
					}
				?>
				<!--<a href="fb-messenger://share/?link= https%3A%2F%2F10mails.net&app_id=123456789"><span class="all-icons facebook-messenger-icon">Messenger</span></a>-->
				<a href="https://t.me/share/url?url=<?=$lang["share_txt"]?>" target="_blank"><span class="all-icons telegram-icon">Telegram</span></a>
				<a href="https://3p3x.adj.st/?adjust_t=u783g1_kw9yml&adjust_fallback=https%3A%2F%2Fwww.viber.com%2F%3Futm_source%3DPartner%26utm_medium%3DSharebutton%26utm_campaign%3DDefualt&adjust_campaign=Sharebutton&adjust_deeplink=viber://forward?text=<?=$lang["share_txt"]?>" target="_blank"><span class="all-icons viber-icon">Viber</span></a>
				<span class="all-icons facebook-icon pntr" onclick="share_on('facebook')">Facebook</span>
				<span class="all-icons twitter-icon pntr" onclick="share_on('twitter')">Twitter</span>
				<span class="all-icons vk-icon pntr" onclick="share_on('vk')">VK</span>
				<span class="all-icons pinterest-icon pntr" onclick="share_on('pinterest')">Pinterest</span>
		</div>
	</div>

	<!-- POPUP_NOTIFICATION -->

	<div id="notification" class="popup-notification">
		<span class="notification"><?=$lang["time_extended"]?></span>
	</div>

	<script src="/js/main.js"></script>

	<script type="text/javascript">

		//setTimeout(function(){document.getElementById("popup_share_us").style.display = "block";}, 30000);
		//setTimeout(function(){document.getElementById("popup_share_us").classList.add("popup-effect");}, 30030);

		function show_modal(element_id) {
			var e = document.getElementById(element_id);
			//console.log(e);
			var modal_bg = document.getElementById("modal_bg");
			modal_bg.style.display = "block";
			e.style.display = "block";
			document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
			setTimeout(function(){modal_bg.classList.add("modal-effect");}, 0);
			setTimeout(function(){e.classList.add("modal-content-effect");}, 0);
		}
		function show_modal_x() {
			var e = document.getElementById("modal_mail");
			var modal_bg = document.getElementById("modal_bg");
			modal_bg.style.display = "block";
			e.style.display = "block";
			document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
			setTimeout(function(){modal_bg.classList.add("modal-effect");}, 0);
			setTimeout(function(){e.classList.add("modal-content-effect");}, 0);
		}
		function close_modal(element_id) {
			var e = document.getElementById(element_id);
			var modal_bg = document.getElementById("modal_bg");
			modal_bg.classList.remove("modal-effect");
			e.classList.remove("modal-content-effect");
			document.getElementsByTagName("BODY")[0].style.overflow = "auto";
			setTimeout(function(){modal_bg.style.display = "none";}, 300);
			setTimeout(function(){e.style.display = "none";}, 300);
		}
		function close_modal_popup(element_id) {
			element_id.parentElement.parentElement.style.display = "none";
			element_id.parentElement.parentElement.style.opacity = "0";
		}
		function close_modal_popup_effect(element_id) {
			setTimeout(function(){element_id.parentElement.parentElement.classList.remove("popup-effect");}, 30);
		}

		function check_scroll() {
			if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
				document.getElementById("container").style.height = "70px";
				//document.getElementById("logo").style.height = "30px";
				document.getElementById("logo").style.marginTop = "13px";
				document.getElementById("logo").style.width = "160px";
				document.getElementById("lang-frame").style.lineHeight = "70px";
				document.getElementById("notification").classList.remove("mt90");
			}
			else {
				document.getElementById("container").style.height = "90px";
				//document.getElementById("logo").style.height = "40px";
				document.getElementById("logo").style.marginTop = "23px";
				document.getElementById("logo").style.width = "200px";
				document.getElementById("lang-frame").style.lineHeight = "90px";
				document.getElementById("notification").classList.add("mt90");
			}
		}
		check_scroll();
		window.onscroll = function(){check_scroll()};

		var copy = function(elementId) {

			var copy_text = document.getElementById(elementId).innerHTML;
			var text_area = document.createElement("TEXTAREA");
			text_area.style.fontSize = "16px";
			text_area.value = copy_text;
			document.body.appendChild(text_area);

			var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);

			if (isiOSDevice) {

				var editable = text_area.contentEditable;
				var readOnly = text_area.readOnly;

				text_area.contentEditable = true;
				text_area.readOnly = false;

				var range = document.createRange();
				range.selectNodeContents(text_area);

				var selection = window.getSelection();
				selection.removeAllRanges();
				selection.addRange(range);

				text_area.setSelectionRange(0, 999999);
				text_area.contentEditable = editable;
				text_area.readOnly = readOnly;

			}
			else {
				text_area.select();
			}

			document.execCommand("copy");

			document.body.removeChild(text_area);
			document.getElementById("copy_email").innerHTML = "<?=$lang["copied"]?>";
			document.getElementById("signup_to").style.display = "block";
			setTimeout(function(){document.getElementById("signup_to").style.opacity = "1";}, 1);
		}

		var hmmi = document.getElementById("hmmi");
		setInterval(function(){hmmi.style.backgroundSize = "23px"}, 1000);
		setTimeout(function(){setInterval(function(){hmmi.style.backgroundSize = "19px"}, 1000)}, 500);

		document.onclick = function(e) {
			if (e.target.id == "modal_about")
				close_modal('modal_about');
			if (e.target.id == "modal_lang")
				close_modal('modal_lang');
			if (e.target.id == "modal_mail")
				close_modal('modal_mail');
		}

		// Share

		function share_on(where) {
			if (where == "facebook")
				window.open("https://www.facebook.com/sharer/sharer.php?u=10mails.net", "pop", "width=600, height=400, scrollbars=no");
			else if (where == "twitter")
				window.open("https://twitter.com/share?url=https://10mails.net&text=<?=$lang["share_txt_nourl"]?>", "pop", "width=600, height=400, scrollbars=no");
			else if (where == "vk")
				window.open("https://vk.com/share.php?url=10mails.net&title=<?=$lang["share_txt"]?>", "pop", "width=600, height=400, scrollbars=no");
			else if (where == "pinterest")
				window.open("http://pinterest.com/pin/create/link/?url=10mails.net", "pop", "width=600, height=400, scrollbars=no");
		}


	</script>
	<script src="js/app.js"></script>
	<script type="text/javascript">
	 if ('serviceWorker' in navigator) {
	    console.log("Will the service worker register?");
	    navigator.serviceWorker.register('service-worker.js')
	      .then(function(reg){
	        console.log("Yes, it did.");
	     }).catch(function(err) {
	        console.log("No it didn't. This happened:", err)
	    });
	 }
	</script>

</body>
</html>