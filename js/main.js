﻿// COUNTDOWN

var x = (date1 - date2) / 1000;
x--;
var clck = setInterval(function(){x--}, 1000);
if (x <= 0) {
	clearInterval(clck);
	x = 0;
}
var y = 100;

function start_countdown() {
	cdown = setInterval("countdown()", 1000);
	cdown2 = setInterval("countdown2()", 10);
}

function countdown() {
	var omin1 = document.getElementById("min1");
	var omin0 = document.getElementById("min0");
	var osec1 = document.getElementById("sec1");
	var osec0 = document.getElementById("sec0");
	var ossec1 = document.getElementById("ssec1");
	var ossec0 = document.getElementById("ssec0");
	var oreagent1 = document.getElementById("reagent1");
	var oreagent0 = document.getElementById("reagent0");
	var countdown = document.getElementById("countdown");

	var sec = Math.floor(x % 60);
	var min = Math.floor(x / 60 % 60);

	if (sec < 10)
		sec = "0" + sec;
	if (min < 10)
		min = "0" + min;

	var sec0 = sec.toString().substr(1, 1);
	var sec1 = sec.toString().substr(0, 1);
	var min0 = min.toString().substr(1, 1);
	var min1 = min.toString().substr(0, 1);

	omin1.innerHTML = min1;
	omin0.innerHTML = min0;
	osec1.innerHTML = sec1;
	osec0.innerHTML = sec0;

	if (x < 1.5 * 60) {
		omin1.style.color = "#e02a2a";
		omin0.style.color = "#e02a2a";
		osec1.style.color = "#e02a2a";
		osec0.style.color = "#e02a2a";
		ossec1.style.color = "#e02a2a";
		ossec0.style.color = "#e02a2a";
		oreagent0.style.color = "#e02a2a";
		oreagent1.style.color = "#e02a2a";
	}

	if (x <= 0) {
		clearInterval(cdown);
		clearInterval(clck);
		y = 100;
		setTimeout(function(){
			clearInterval(cdown2);
			ossec1.innerHTML = 0;
			ossec0.innerHTML = 0;
		}, 1000);
	}
	else {
		y = 100;
	}
}

function countdown2() {
	var ossec1 = document.getElementById("ssec1");
	var ossec0 = document.getElementById("ssec0");

	var ssec = y % 100;

	if (ssec < 10)
		ssec = "0" + ssec;

	var ssec0 = ssec.toString().substr(1, 1);
	var ssec1 = ssec.toString().substr(0, 1);

	ossec1.innerHTML = ssec1;
	ossec0.innerHTML = ssec0;

	y--;
}
countdown();
countdown2();
document.body.onload = function(){start_countdown()};