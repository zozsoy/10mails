<?php
ob_start();
require_once "functions.php";
	//Random mail name and cookie
    function create_random_mail(){
    	$random_name = randomName();
        do{
            $result=database_compare($random_name,"ktumail.com",true);
            if(!$result)
                $random_name = randomName();
        }while(!$result);
        $db_result=save_to_db($random_name,"ktumail.com");
        if($db_result)
            die();
        setcookie("TMA",$random_name,time()+60*10);//TMA: Temporary Mail Address

        return  $random_name . "@ktumail.com";
    }
	if(!isset($_COOKIE['TMA'])){
        $tma_name = create_random_mail();
	}
	else{
		$result = database_compare(trim($_COOKIE['TMA']),"ktumail.com",false);
		$user = compare_right_user(trim($_COOKIE['TMA']),"ktumail.com", getUserIpAddr());
        if(!$result || empty(trim($_COOKIE['TMA'])) || !$user)
            $tma_name =create_random_mail();
        else
            $tma_name = $_COOKIE['TMA']."@ktumail.com";
	}

	$browser_lg = substr(locale_accept_from_http($_SERVER["HTTP_ACCEPT_LANGUAGE"]), 0 ,2);
	$lg = $_GET["lang"];

	if (!isset($_COOKIE["LANG"]) || ($_COOKIE["LANG"] != "id" && $_COOKIE["LANG"] != "de" && $_COOKIE["LANG"] != "en" && $_COOKIE["LANG"] != "es" && $_COOKIE["LANG"] != "fr" && $_COOKIE["LANG"] != "it" && $_COOKIE["LANG"] != "nl" && $_COOKIE["LANG"] != "no" && $_COOKIE["LANG"] != "pt" && $_COOKIE["LANG"] != "sv" && $_COOKIE["LANG"] != "tr" && $_COOKIE["LANG"] != "ru" && $_COOKIE["LANG"] != "uk" && $_COOKIE["LANG"] != "hi" && $_COOKIE["LANG"] != "zh-cn" && $_COOKIE["LANG"] != "ja")) {
		if ($browser_lg == "id" || $browser_lg == "de" || $browser_lg == "en" || $browser_lg == "es" || $browser_lg == "fr" || $browser_lg == "it" || $browser_lg == "nl" || $browser_lg == "no" || $browser_lg == "pt" || $browser_lg == "sv" || $browser_lg == "tr" || $browser_lg == "ru" || $browser_lg == "uk" || $browser_lg == "hi" || $browser_lg == "zh" || $browser_lg == "ja") {
			setcookie("LANG", $browser_lg, time()+60*60*24*365);
			$_COOKIE["LANG"] = $browser_lg;
		}
		else {
			setcookie("LANG", "en", time()+60*60*24*365);
			$_COOKIE["LANG"] = "en";
		}
	}

	if ($lg == "id" || $lg == "de" || $lg == "en" || $lg == "es" || $lg == "fr" || $lg == "it" || $lg == "nl" || $lg == "no" || $lg == "pt" || $lg == "sv" || $lg == "tr" || $lg == "ru" || $lg == "uk" || $lg == "hi" || $lg == "zh-cn" || $lg == "ja") {
		setcookie("LANG", $lg, time()+60*60*24*365);
		$_COOKIE["LANG"] = $lg;
	}

	$created_mail_count = number_format(how_many_mail());

	include "languages/gen.php";
	include "languages/lang.php";
?>
<!DOCTYPE html>
<html lang="<?=$_COOKIE["LANG"]?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/main.css" type="text/css" />
	<link rel="stylesheet" href="css/pgs.css" type="text/css" />
	<link rel="icon" type="image/png" sizes="64x64" href="css/icons/favicon.png">
	<link rel="apple-touch-icon" sizes="180x180" href="css/icons/apple-touch-icon.png">
	<meta name="msapplication-square70x70logo" content="css/icons/ms-70.png">
	<meta name="msapplication-square150x150logo" content="css/icons/ms-150.png">
	<meta name="msapplication-square310x310logo" content="css/icons/ms-310.png">
	<meta name="msapplication-wide310x150logo" content="css/icons/ms-rect-310.png">
	<meta name="msapplication-TileColor" content="#ebeff6">
	<title><?=$lang["privacy"]?> - 10mails.net</title>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140125866-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-140125866-1');
	</script>
	<!-- Start Alexa Certify Javascript -->
	<script type="text/javascript">
	_atrk_opts = { atrk_acct:"brGkt1Y1Mn20Io", domain:"10mails.net",dynamic: true};
	(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
	</script>
	<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=brGkt1Y1Mn20Io" style="display:none" height="1" width="1" alt="" /></noscript>
	<!-- End Alexa Certify Javascript -->
</head>
<body>
		



	<!-- CONTAINER -->


	<?php include 'ktu_container.php'; ?>





	<!-- BODY -->


	<div class="main">

		<div class="w-headline">
			<h1>Privacy Policy</h1>
			<p class="pr mt0">Last updated on 17 May, 2019</p>
			<div class="tbody">
			<h2>Definitions</h2>
			<p class="pr">With this privacy policy, the use and protection of personal information of users on 10mails.net is organized.</p>
			<p class="pr">Personal data of users, name, surname, date of birth, telephone number, e-mail address, IP address, such as any personal data directly or indirectly to identify the user and this privacy policy "personal data" will be referred to as.</p>
			<p class="pr">The personal data of 10mails.net users will not be used in any way except the basis and scope determined by this privacy policy and will not be shared with third parties.</p>
			<h2>What data do we collect?</h2>
			<p class="pr">We do not collect any personal data of the user. All email addresses you have received through 10mails.net are permanently deleted after 10 minutes unless the term is extended. The names of the email addresses that are created are stored to avoid being given to others.</p>
			<p class="pr">10mails.net can obtain statistical information from users who use the website by using a technical contact file. Users can change the browser settings to prevent this file if they want.</p>
			<h2>Do we use cookies?</h2>
			<p class="pr">Yes. Cookies are information stored on your computer by a website that you visit. 10mails.net uses cookies to increase user experience and provide more personal results to the user.</p>
			<p class="pr mt40">Any user who visits the 10mails.net website is deemed to have accepted the 10mails.net privacy policy.</p>
			</div>
		</div>

	</div>


	<!-- FOOTER -->

	<?php include 'ktu_footer.php'; ?>

	<!-- MODAL_LANG -->

	<div id="modal_lang" class="modal-main">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="modal-h1"><?=$lang["languages"]?></h1>
				<div onclick="close_modal('modal_lang')" class="close-icon"></div>
			</div>
			<div class="modal-body">
				<div class="lg-list">
					<a href="?lang=id"><p class="modal-p"><?=$lang["indonesian"]?></p></a>
					<a href="?lang=de"><p class="modal-p"><?=$lang["german"]?></p></a>
					<a href="?lang=en"><p class="modal-p"><?=$lang["english"]?></p></a>
					<a href="?lang=es"><p class="modal-p"><?=$lang["spanish"]?></p></a>
					<a href="?lang=fr"><p class="modal-p"><?=$lang["french"]?></p></a>
					<a href="?lang=it"><p class="modal-p"><?=$lang["italian"]?></p></a>
					<a href="?lang=nl"><p class="modal-p"><?=$lang["dutch"]?></p></a>
					<a href="?lang=no"><p class="modal-p"><?=$lang["norwegian"]?></p></a>
				</div>
				<div class="lg-list">
					<a href="?lang=pt"><p class="modal-p"><?=$lang["portuguese"]?></p></a>
					<a href="?lang=sv"><p class="modal-p"><?=$lang["swedish"]?></p></a>
					<a href="?lang=tr"><p class="modal-p"><?=$lang["turkish"]?></p></a>
					<a href="?lang=ru"><p class="modal-p"><?=$lang["russian"]?></p></a>
					<a href="?lang=uk"><p class="modal-p"><?=$lang["ukranian"]?></p></a>
					<a href="?lang=hi"><p class="modal-p"><?=$lang["hindu"]?></p></a>
					<a href="?lang=zh-cn"><p class="modal-p"><?=$lang["chinese-simplified"]?></p></a>
					<a href="?lang=ja"><p class="modal-p"><?=$lang["japanese"]?></p></a>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL_BG -->

	<div id="modal_bg" class="modal"></div>

	<script type="text/javascript">

		function show_modal(element_id) {
			var e = document.getElementById(element_id);
			var modal_bg = document.getElementById("modal_bg");
			modal_bg.style.display = "block";
			e.style.display = "block";
			document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
			setTimeout(function(){modal_bg.classList.add("modal-effect");}, 0);
			setTimeout(function(){e.classList.add("modal-content-effect");}, 0);
		}
		function close_modal(element_id) {
			var e = document.getElementById(element_id);
			var modal_bg = document.getElementById("modal_bg");
			modal_bg.classList.remove("modal-effect");
			e.classList.remove("modal-content-effect");
			document.getElementsByTagName("BODY")[0].style.overflow = "auto";
			setTimeout(function(){modal_bg.style.display = "none";}, 300);
			setTimeout(function(){e.style.display = "none";}, 300);
		}
		function close_modal_popup(element_id) {
			element_id.parentElement.parentElement.style.display = "none";
			element_id.parentElement.parentElement.style.opacity = "0";
		}
		function close_modal_popup_effect(element_id) {
			setTimeout(function(){element_id.parentElement.parentElement.classList.remove("popup-effect");}, 30);
		}

		function check_scroll() {
			if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
				document.getElementById("container").style.height = "70px";
				//document.getElementById("logo").style.height = "30px";
				document.getElementById("logo").style.marginTop = "13px";
				document.getElementById("logo").style.width = "160px";
				document.getElementById("lang-frame").style.lineHeight = "70px";
			}
			else {
				document.getElementById("container").style.height = "90px";
				//document.getElementById("logo").style.height = "40px";
				document.getElementById("logo").style.marginTop = "23px";
				document.getElementById("logo").style.width = "200px";
				document.getElementById("lang-frame").style.lineHeight = "90px";
			}
		}
		check_scroll();
		window.onscroll = function(){check_scroll()};

		var hmmi = document.getElementById("hmmi");
		setInterval(function(){hmmi.style.backgroundSize = "23px"}, 1000);
		setTimeout(function(){setInterval(function(){hmmi.style.backgroundSize = "19px"}, 1000)}, 500);

		document.onclick = function(e) {
			if (e.target.id == "modal_about")
				close_modal('modal_about');
			if (e.target.id == "modal_lang")
				close_modal('modal_lang');
			if (e.target.id == "modal_mail")
				close_modal('modal_mail');
		}

	</script>

</body>
</html>