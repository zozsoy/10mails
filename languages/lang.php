<?php
	error_reporting(0); /*Noties*/
	
	$lang = array();


	//GENERAL BEFORE

	$lang["indonesian"] = "Bahasa Indonesia";
	$lang["german"] = "Deutsch";
	$lang["english"] = "English";
	$lang["spanish"] = "Español";
	$lang["french"] = "Français";
	$lang["italian"] = "Italiano";
	$lang["dutch"] = "Nederlands";
	$lang["norwegian"] = "Norsk";
	$lang["portuguese"] = "Português";
	$lang["swedish"] = "Svenska";
	$lang["turkish"] = "Türkçe";
	$lang["russian"] = "Русский";
	$lang["ukranian"] = "Українська";
	$lang["hindu"] = "हिंदू";
	$lang["chinese-simplified"] = "中文(简体)";
	$lang["japanese"] = "日本語";


	$lang["copyright"] = "10mails.net © 2020";



	//Head TAG
			$lang["title_tag"] = "10 Minute Temporary Mail - 10mails.net";
			$lang["meta_description"] = "Get a new email account without need to signup with 10 minute temp mail and keep your privacy online.";

	//MAIN

		//Container
			$lang["language"] = "English";
			$lang["language_code"] = "En";

		//Body
			$lang["welcome"] = "Welcome!";
			$lang["this_is_your"] = "This is your temporary email address.";
			$lang["signup_with_this"] = "Signup with this email to:";
			$lang["inbox"] = "Inbox";

			if ($mail_count > 1)
				$lang["total_mail"] = "You have <b></b> emails in total.";
			else
				$lang["total_mail"] = "You have <b></b> email in total.";

			$lang["no_email"] = "You have no email yet.";

		//SEO
			$lang["seo_what_is"] = "What is 10 Minute Temporary Mail?";
			$lang["seo_p1"] = "Disposable email addresses that self-destruct after a certain period of time. You don't need to register and login to use this service. It is also known as <b>temp mail</b>, <b>10 minute mail</b>, <b>disposable email</b>, <b>guerrilla mail</b>, <b>throwaway email</b> or <b>fake mail</b>.";
			$lang["seo_p2"] = "You will need to provide your email address to perform many operations on the Internet. To access content on forum sites, download a file or use a service, you will have to signup to that site. This will cause your inbox to be filled with spam emails. Temporary disposable email is the best way to protect against spam mails.";
			$lang["seo_safe"] = "Safe";
			$lang["seo_safe_p"] = "Email addresses are for single use only and can't be accessed by anyone other than you.";
			$lang["seo_fast"] = "Fast";
			$lang["seo_fast_p"] = "Emails will drop into your inbox as soon as possible. So you don't have to wait.";
			$lang["seo_ex_time"] = "Extendable Time";
			$lang["seo_ex_time_p"] = "If you say 10 minutes is not enough for me, you can extend the time by one hour.";

		//Footer
			$lang["about"] = "About";
			$lang["blog"] = "Blog";
			$lang["privacy"] = "Privacy";
			$lang["terms"] = "Terms";
			$lang["emails_created"] = "<b>" . $created_mail_count . "</b> emails created.";

	//TOOLTIPS

			$lang["what_is_this"] = "What is this?";
			$lang["copy"] = "Copy";
			$lang["copied"] = "Copied!";
			$lang["extend_time"] = "Extend the time";
			$lang["refresh"] = "Refresh";

	//MODALS

		//Languages
			$lang["languages"] = "Languages";
			// $lang["english"] = "<b>English</b>";

		//About
			$lang["whats_10mails"] = "What is 10mails.net?";
			$lang["10mails_is"] = "10mails.net is a temporary email service. We created it for you to stay anonymous on the internet and get rid of junk emails with deleted email addresses at the end of a certain period of time.";
			$lang["10mails_is_2"] = "We've created <b>" . $tma_name . "</b> address for you. Nobody used it before you, and we won't give it to anyone after you. By default, we give each email address 10 minutes. You can extend the time for 1 hour by clicking on the \"<span class=\"giveme60btn\">.</span>\" button if you want. We permanently delete expired email addresses with emails.";
			$lang["where_can_i_use"] = "Where can I use it?";
			$lang["you_can_use"] = "Maybe to receive a confirmation email from a site you are a member of or to open multiple memberships on the same site. You can use it wherever you don't want to use your personal email address. With this method, you don't always have to open a new email address and you save time.";
			$lang["got_it"] = "Got it";

	//POPUPS

		//Share
			$lang["do_u_like"] = "<b>Do you like our app?</b> Let your friends also know 😊";
			$lang["share_txt_nourl"] = "Get%20a%20new%20email%20account%20without%20need%20to%20signup%20with%2010%20minute%20temporary%20mail%20and%20keep%20your%20privacy%20online.%20It%27s%20completely%20free%21%0ATry%20it%20now";

		//Notifications
			$lang["error"] = "Error.";
			$lang["time_extended"] = "Time extended by 1 hour.";
			$lang["time_extended_err"] = "Time doesn't extended. Please try again.";







	//INDONESIAN


			if ($_COOKIE["LANG"] == "id") {

				//Head TAG
						$lang["title_tag"] = "Email 10 Menit - 10mails.net";
						$lang["meta_description"] = "Dapatkan akun email baru dengan 10 menit mail sementara dan jaga privasi Anda online.";

				//MAIN

					//Container
						$lang["language"] = "Bahasa Indonesia";
						$lang["language_code"] = "Id";

					//Body
						$lang["welcome"] = "Selamat datang!";
						$lang["this_is_your"] = "Ini adalah alamat email sementara Anda.";
						$lang["signup_with_this"] = "Daftar dengan email ini ke:";
						$lang["inbox"] = "Kotak masuk";
						$lang["total_mail"] = "Anda memiliki <b></b> email secara total.";
						$lang["no_email"] = "Anda belum memiliki email.";

					//SEO
						$lang["seo_what_is"] = "Apa itu Email Sementara 10 Menit?";
						$lang["seo_p1"] = "Alamat email sekali pakai yang dapat dihancurkan sendiri setelah periode waktu tertentu. Anda tidak perlu mendaftar dan masuk untuk menggunakan layanan ini. Ia juga dikenal sebagai <b>temp mail</b>, <b>10 minute mail</b>, <b>email sekali pakai</b>, <b>guerrilla mail</b>, <b>email sekali pakai</b> atau <b>surat palsu</b>.";
						$lang["seo_p2"] = "Anda harus memberikan alamat email Anda untuk melakukan banyak operasi di Internet. Untuk mengakses konten di situs forum, mengunduh file atau menggunakan layanan, Anda harus berlangganan ke situs itu. Ini akan menyebabkan kotak masuk Anda diisi dengan email spam. Email sekali pakai sementara adalah cara terbaik untuk melindungi terhadap email spam.";
						$lang["seo_safe"] = "Aman";
						$lang["seo_safe_p"] = "Alamat email hanya untuk sekali pakai dan tidak dapat diakses oleh orang lain selain Anda.";
						$lang["seo_fast"] = "Cepat";
						$lang["seo_fast_p"] = "Email akan masuk ke kotak masuk Anda sesegera mungkin. Jadi kamu tidak perlu menunggu.";
						$lang["seo_ex_time"] = "Waktu yang Diperpanjang";
						$lang["seo_ex_time_p"] = "Jika Anda mengatakan 10 menit tidak cukup, Anda dapat memperpanjang waktu satu jam.";

					//Footer
						$lang["about"] = "Tentang";
						$lang["privacy"] = "Pribadi";
						$lang["terms"] = "Ketentuan";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> email dibuat.";

				//TOOLTIPS

						$lang["what_is_this"] = "Apa ini?";
						$lang["copy"] = "Salinan";
						$lang["copied"] = "Disalin!";
						$lang["extend_time"] = "Perpanjang waktu";
						$lang["refresh"] = "Menyegarkan";

				//MODALS

					//Languages
						$lang["languages"] = "Bahasa";
						$lang["indonesian"] = "<b>Bahasa Indonesia</b>";

					//About
						$lang["whats_10mails"] = "Apa itu 10mails.net?";
						$lang["10mails_is"] = "10mails.net adalah layanan email sementara. Kami menciptakannya untuk Anda menyelesaikan pekerjaan dan membuang email sampah dengan alamat email yang dihapus pada akhir periode waktu tertentu.";
						$lang["10mails_is_2"] = "Kami membuat alamat <b>" . $tma_name . "</b> untuk Anda. Tidak ada yang menggunakannya sebelum Anda, dan kami tidak akan memberikannya kepada siapa pun setelah Anda. Secara default, kami memberikan setiap alamat email 10 menit. Anda dapat memperpanjang waktu selama 1 jam dengan mengklik tombol \"<span class=\"giveme60btn\">.</span>\" jika Anda mau. Kami secara permanen menghapus alamat email kadaluwarsa dengan email.";
						$lang["where_can_i_use"] = "Dimana saya bisa menggunakannya?";
						$lang["you_can_use"] = "Mungkin untuk menerima email konfirmasi dari situs Anda adalah anggota atau untuk membuka banyak keanggotaan di situs yang sama. Dengan metode ini, Anda tidak selalu harus membuka alamat email baru dan menghemat waktu.";
						$lang["got_it"] = "Memahami";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Apakah Anda menyukai aplikasi kami?</b> Beri tahu teman Anda juga 😊";
						$lang["share_txt_nourl"] = "Dapatkan%20akun%20email%20baru%20dengan%2010%20menit%20email%20sementara%20dan%20jaga%20privasi%20Anda%20online.%20Dan%20sepenuhnya%20gratis%21%0ACobalah%20di";

					//Notifications
						$lang["error"] = "Kesalahan.";
						$lang["time_extended"] = "Waktu diperpanjang 1 jam.";
						$lang["time_extended_err"] = "Waktu tidak diperpanjang. Silakan coba lagi.";

			}







	//GERMAN


			else if ($_COOKIE["LANG"] == "de") {

				//Head TAG
						$lang["title_tag"] = "10 Minuten Mail - 10mails.net";
						$lang["meta_description"] = "Eröffnen Sie ein neues E-Mail-Konto mit einer 10 Minuten gültigen temp mail und schützen Sie Ihre Privatsphäre im Internet.";

				//MAIN

					//Container
						$lang["language"] = "Deutsch";
						$lang["language_code"] = "De";

					//Body
						$lang["welcome"] = "Willkommen!";
						$lang["this_is_your"] = "Dies ist Ihre temporäre E-Mail-Adresse.";
						$lang["signup_with_this"] = "Melden Sie sich mit dieser E-Mail an:";
						$lang["inbox"] = "Posteingang";

						if ($mail_count > 1)
							$lang["total_mail"] = "Sie haben insgesamt <b></b> E-Mails.";
						else
							$lang["total_mail"] = "Sie haben insgesamt <b></b> E-Mail.";

						$lang["no_email"] = "Du hast noch keine E-Mail.";

					//SEO
						$lang["seo_what_is"] = "Was ist der Temporäre 10 Minuten E-Mail Dienst?";
						$lang["seo_p1"] = "Sie erhalten Einweg-E-Mail-Adressen, die sich nach einer bestimmten Zeit selbst zerstören. Sie müssen sich nicht registrieren und anmelden, um diesen Service zu nutzen. Dieser Dienst ist auch bekannt als <b>Temporäre Post</b>, <b>10-Minuten-Post</b>, <b>Wegwerf-Post</b>, <b>Guerilla-Post</b>, <b>Wegwerf-Post</b> oder <b>Gefälschte Post</b>.";
						$lang["seo_p2"] = "Sie müssen Ihre E-Mail-Adresse angeben, um viele Vorgänge im Internet ausführen zu können. Um auf Inhalte auf Forenseiten zuzugreifen, eine Datei herunterzuladen oder einen Dienst zu nutzen, verlangen diese Seiten Abos. Dadurch wird Ihr Posteingang mit Spam-E-Mails gefüllt. Temporäre Wegwerf-E-Mails sind der beste Schutz gegen Spam-Mails.";
						$lang["seo_safe"] = "Sicher";
						$lang["seo_safe_p"] = "E-Mail-Adressen sind nur für den einmaligen Gebrauch bestimmt und können nur von Ihnen aufgerufen werden.";
						$lang["seo_fast"] = "Schnell";
						$lang["seo_fast_p"] = "E-Mails werden so schnell wie möglich in Ihrem Posteingang abgelegt. Sie müssen also nicht warten.";
						$lang["seo_ex_time"] = "Verlängerbare Zeit";
						$lang["seo_ex_time_p"] = "Wenn Sie denken, dass 10 Minuten nicht ausreichen, können Sie die Zeit um eine Stunde verlängern.";

					//Footer
						$lang["about"] = "Über";
						$lang["privacy"] = "Privatsphäre";
						$lang["terms"] = "Bedingungen";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> E-Mails wurden erstellt.";

				//TOOLTIPS

						$lang["what_is_this"] = "Was ist das?";
						$lang["copy"] = "Kopieren";
						$lang["copied"] = "Kopiert!";
						$lang["extend_time"] = "Die Zeit verlängern";
						$lang["refresh"] = "Aktualisierung";

				//MODALS

					//Languages
						$lang["languages"] = "Sprachen";
						$lang["german"] = "<b>Deutsch</b>";

					//About
						$lang["whats_10mails"] = "Was ist 10mails.net?";
						$lang["10mails_is"] = "10mails.net ist ein temporärer E-Mail-Dienst. Die erstellte E-Mail-Adresse wird nach kurzer Zeit wieder gelöscht. So können Sie, während Sie im Internet etwas erledigen, anonym bleiben und lästige Junk-E-Mails vermeiden.";
						$lang["10mails_is_2"] = "Wir haben die E-Mail-Adresse <b>" . $tma_name . "</b> persönlich für Sie erstellt. Vor Ihnen hat es niemand genutzt und danach wird es auch niemand anderem vergeben. Wenn Sie möchten, können Sie die Zeit um 60 Minuten verlängern, indem Sie auf die Schaltfläche \"<span class=\"giveme60btn\">.</span>\" klicken. Wir löschen abgelaufene E-Mail-Adressen samt der darin befindlichen E-Mails dauerhaft.";
						$lang["where_can_i_use"] = "Wozu kann ich es nutzen?";
						$lang["you_can_use"] = "Die temporäre E-Mail-Adresse können Sie beispielsweise nutzen um eine Bestätigungsmail zu empfangen oder sich auf einer Internetseite mehrmals zu registrieren. So müssen Sie nicht jedesmal eine neue E-Mail-Adresse erstellen und können Zeit sparen.";
						$lang["got_it"] = "Habe Verstanden";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Gefällt dir unsere App?</b> Lass es auch deine Freunde wissen 😊";
						$lang["share_txt_nourl"] = "Er%C3%B6ffnen%20Sie%20ein%20neues%20E-Mail-Konto%20mit%20einer%2010%20Minuten%20g%C3%BCltigen%20tempor%C3%A4ren%20E-Mail-Adresse%20und%20sch%C3%BCtzen%20Sie%20Ihre%20Privatsph%C3%A4re%20im%20Internet.%20Es%20ist%20v%C3%B6llig%20kostenlos%21%0AVersuchen%20Sie%20es%20unter";

					//Notifications
						$lang["error"] = "Error.";
						$lang["time_extended"] = "Zeit um 1 Stunde verlängert.";
						$lang["time_extended_err"] = "Die Zeit verlängert sich nicht. Bitte versuche es erneut.";

			}







	//ENGLISH


			else if ($_COOKIE["LANG"] == "en") {

				//Head TAG
						$lang["title_tag"] = "10 Minute Temporary Mail - 10mails.net";
						$lang["meta_description"] = "Get a new email account without need to signup with 10 minute temp mail and keep your privacy online.";

				//MAIN

					//Container
						$lang["language"] = "English";
						$lang["language_code"] = "En";

					//Body
						$lang["welcome"] = "Welcome!";
						$lang["this_is_your"] = "This is your temporary email address.";
						$lang["signup_with_this"] = "Signup with this email to:";
						$lang["inbox"] = "Inbox";

						if ($mail_count > 1)
							$lang["total_mail"] = "You have <b></b> emails in total.";
						else
							$lang["total_mail"] = "You have <b></b> email in total.";

						$lang["no_email"] = "You have no email yet.";

					//SEO
						$lang["seo_what_is"] = "What is 10 Minute Temporary Mail?";
						$lang["seo_p1"] = "Disposable email addresses that self-destruct after a certain period of time. You don't need to register and login to use this service. It is also known as <b>temp mail</b>, <b>10 minute mail</b>, <b>disposable email</b>, <b>guerrilla mail</b>, <b>throwaway email</b> or <b>fake mail</b>.";
						$lang["seo_p2"] = "You will need to provide your email address to perform many operations on the Internet. To access content on forum sites, download a file or use a service, you will have to signup to that site. This will cause your inbox to be filled with spam emails. Temporary disposable email is the best way to protect against spam mails.";
						$lang["seo_safe"] = "Safe";
						$lang["seo_safe_p"] = "Email addresses are for single use only and can't be accessed by anyone other than you.";
						$lang["seo_fast"] = "Fast";
						$lang["seo_fast_p"] = "Emails will drop into your inbox as soon as possible. So you don't have to wait.";
						$lang["seo_ex_time"] = "Extendable Time";
						$lang["seo_ex_time_p"] = "If you say 10 minutes is not enough for me, you can extend the time by one hour.";

					//Footer
						$lang["about"] = "About";
						$lang["privacy"] = "Privacy";
						$lang["terms"] = "Terms";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> emails created.";

				//TOOLTIPS

						$lang["what_is_this"] = "What is this?";
						$lang["copy"] = "Copy";
						$lang["copied"] = "Copied!";
						$lang["extend_time"] = "Extend the time";
						$lang["refresh"] = "Refresh";

				//MODALS

					//Languages
						$lang["languages"] = "Languages";
						$lang["english"] = "<b>English</b>";

					//About
						$lang["whats_10mails"] = "What is 10mails.net?";
						$lang["10mails_is"] = "10mails.net is a temporary email service. We created it for you to stay anonymous on the internet and get rid of junk emails with deleted email addresses at the end of a certain period of time.";
						$lang["10mails_is_2"] = "We've created <b>" . $tma_name . "</b> address for you. Nobody used it before you, and we won't give it to anyone after you. By default, we give each email address 10 minutes. You can extend the time for 1 hour by clicking on the \"<span class=\"giveme60btn\">.</span>\" button if you want. We permanently delete expired email addresses with emails.";
						$lang["where_can_i_use"] = "Where can I use it?";
						$lang["you_can_use"] = "Maybe to receive a confirmation email from a site you are a member of or to open multiple memberships on the same site. You can use it wherever you don't want to use your personal email address. With this method, you don't always have to open a new email address and you save time.";
						$lang["got_it"] = "Got it";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Do you like our app?</b> Let your friends also know 😊";
						$lang["share_txt_nourl"] = "Get%20a%20new%20email%20account%20without%20need%20to%20signup%20with%2010%20minute%20temporary%20mail%20and%20keep%20your%20privacy%20online.%20It%27s%20completely%20free%21%0ATry%20it%20now";

					//Notifications
						$lang["error"] = "Error.";
						$lang["time_extended"] = "Time extended by 1 hour.";
						$lang["time_extended_err"] = "Time doesn't extended. Please try again.";

			}







	//SPANISH


			else if ($_COOKIE["LANG"] == "es") {

				//Head TAG
						$lang["title_tag"] = "E-mail temporal de 10 minutos - 10mails.net";
						$lang["meta_description"] = "Obtenga una nueva cuenta de e-mail con 10 minutos de mail temporal y mantenga su privacidad en línea.";

				//MAIN

					//Container
						$lang["language"] = "Español";
						$lang["language_code"] = "Es";

					//Body
						$lang["welcome"] = "¡Bienvenido!";
						$lang["this_is_your"] = "Esta es su dirección de e-mail temporal.";
						$lang["signup_with_this"] = "Regístrese con este e-mail a:";
						$lang["inbox"] = "Bandeja de entrada";

						if ($mail_count > 1)
							$lang["total_mail"] = "Tienes <b></b> e-mails en total.";
						else
							$lang["total_mail"] = "Tienes <b></b> e-mail en total.";

						$lang["no_email"] = "Aún no tienes e-mail.";

					//SEO
						$lang["seo_what_is"] = "¿Qué es E-mail Temporal de 10 Minutos?";
						$lang["seo_p1"] = "Direcciones de e-mail desechables que se autodestruyen después de un cierto período de tiempo. No necesita registrarse e iniciar sesión para usar este servicio. También se conoce como <b>email temporal</b>, <b>email de 10 minutos</b>, <b>email electrónico desechable</b>, <b>guerrilla mail</b>, <b>correo electrónico desechable</b> o <b>correo falso</b>.";
						$lang["seo_p2"] = "Deberá proporcionar su dirección de e-mail para realizar muchas operaciones en Internet. Para acceder al contenido de los sitios del foro, descargar un archivo o utilizar un servicio, deberá suscribirse a ese sitio. Esto hará que su bandeja de entrada se llene de correos electrónicos no deseados. El correo electrónico desechable temporal es la mejor manera de protegerse contra los correos no deseados.";
						$lang["seo_safe"] = "Asegurar";
						$lang["seo_safe_p"] = "Las direcciones de e-mail son de un solo uso y nadie más que usted no puede acceder a ellas.";
						$lang["seo_fast"] = "Rápido";
						$lang["seo_fast_p"] = "Los correos electrónicos llegarán a su bandeja de entrada lo antes posible. Entonces no tienes que esperar.";
						$lang["seo_ex_time"] = "Tiempo Extensible";
						$lang["seo_ex_time_p"] = "Si dice que 10 minutos no es suficiente, puede extender el tiempo en una hora.";

					//Footer
						$lang["about"] = "Acerca de";
						$lang["privacy"] = "Intimidad";
						$lang["terms"] = "Condiciones";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-mails creados.";

				//TOOLTIPS

						$lang["what_is_this"] = "¿Que es esto?";
						$lang["copy"] = "Dupdo";
						$lang["copied"] = "Copiado!";
						$lang["extend_time"] = "Extiende el tiempo";
						$lang["refresh"] = "Refrescar";

				//MODALS

					//Languages
						$lang["languages"] = "Idiomas";
						$lang["spanish"] = "<b>Español</b>";

					//About
						$lang["whats_10mails"] = "¿Qué es 10mails.net?";
						$lang["10mails_is"] = "10mails.net es un servicio de e-mail temporal. Lo creamos para que pueda hacer el trabajo y deshacerse de los e-mail no deseados con las direcciones de e-mail eliminadas al final de un cierto período de tiempo.";
						$lang["10mails_is_2"] = "Hemos creado <b>" . $tma_name . "</b> dirección para usted. Nadie lo usó antes que usted, y no se lo daremos a nadie después de usted. Por defecto, le damos a cada dirección de e-mail 10 minutos. Puede ampliar el tiempo de 1 hora haciendo clic en el botón \"<span class=\"giveme60btn\">.</span>\" si lo desea. Eliminamos permanentemente las direcciones de e-mail expiradas con e-mail.";
						$lang["where_can_i_use"] = "¿Dónde lo puedo usar?";
						$lang["you_can_use"] = "Tal vez para recibir un e-mail de confirmación de un sitio del que es miembro o para abrir varias membresías en el mismo sitio. Con este método, no siempre tiene que abrir una nueva dirección de e-mail y ahorrar tiempo.";
						$lang["got_it"] = "Entender";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>¿Te gusta nuestra aplicación?</b> Deja que tus amigos también sepan 😊";
						$lang["share_txt_nourl"] = "Obtenga%20una%20nueva%20cuenta%20de%20e-mail%20con%2010%20minutos%20de%20e-mail%20temporal%20y%20mantenga%20su%20privacidad%20en%20l%C3%ADnea.%20%C2%A1Y%20es%20completamente%20gratis%21%0APru%C3%A9balo%20en";

					//Notifications
						$lang["error"] = "Error.";
						$lang["time_extended"] = "Tiempo extendido por 1 hora.";
						$lang["time_extended_err"] = "El tiempo no se extiende. Inténtalo de nuevo.";

			}







	//FRENCH


			else if ($_COOKIE["LANG"] == "fr") {

				//Head TAG
						$lang["title_tag"] = "Email temporaire de 10 minutes - 10mails.net";
						$lang["meta_description"] = "Obtenez un nouveau compte de messagerie avec 10 minutes de mail temporaire et gardez votre confidentialité en ligne.";

				//MAIN

					//Container
						$lang["language"] = "Français";
						$lang["language_code"] = "Fr";

					//Body
						$lang["welcome"] = "Bienvenue!";
						$lang["this_is_your"] = "Ceci est votre adresse email temporaire.";
						$lang["signup_with_this"] = "Inscrivez-vous avec cet email à:";
						$lang["inbox"] = "Boîte de réception";

						if ($mail_count > 1)
							$lang["total_mail"] = "Vous avez <b></b> emails au total.";
						else
							$lang["total_mail"] = "Vous avez <b></b> email au total.";

						$lang["no_email"] = "Vous n'avez pas encore de email.";

					//SEO
						$lang["seo_what_is"] = "Qu'est-ce Email Temporaire de 10 Minutes?";
						$lang["seo_p1"] = "Adresses emails jetables qui s'autodétruisent après un certain temps. Vous n'avez pas besoin de vous inscrire et de vous connecter pour utiliser ce service. Il est également connu sous le nom de <b>email temporaire</b>, <b>email de 10 minutes</b>, <b>email électronique jetable</b>, <b>email de guérilla</b>, <b>courrier électronique jetable</b> ou <b>faux courrier</b>.";
						$lang["seo_p2"] = "Vous devrez fournir votre adresse électronique pour effectuer de nombreuses opérations sur Internet. Pour accéder au contenu sur les sites de forum, télécharger un fichier ou utiliser un service, vous devez vous abonner à ce site. Cela provoquera le remplissage de votre boîte de réception avec des spams. Les e-mails temporaires à usage unique constituent le meilleur moyen de se protéger contre les spams.";
						$lang["seo_safe"] = "Assurer";
						$lang["seo_safe_p"] = "Les adresses électroniques sont à usage unique et ne sont accessibles à personne d'autre que vous.";
						$lang["seo_fast"] = "Rapide";
						$lang["seo_fast_p"] = "Les email arriveront dans votre boîte de réception dès que possible. Donc tu n'as pas à attendre.";
						$lang["seo_ex_time"] = "Temps Extensible";
						$lang["seo_ex_time_p"] = "Si vous dites que 10 minutes ne suffisent pas, vous pouvez prolonger le temps d'une heure.";

					//Footer
						$lang["about"] = "Sur";
						$lang["privacy"] = "Intimité";
						$lang["terms"] = "Termes";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> emails créés.";

				//TOOLTIPS

						$lang["what_is_this"] = "Qu'est-ce que c'est?";
						$lang["copy"] = "Copie";
						$lang["copied"] = "Copié!";
						$lang["extend_time"] = "Prolonger le temps";
						$lang["refresh"] = "Rafraîchir";

				//MODALS

					//Languages
						$lang["languages"] = "Les langues";
						$lang["french"] = "<b>Français</b>";

					//About
						$lang["whats_10mails"] = "Qu'est ce que 10mails.net?";
						$lang["10mails_is"] = "10mails.net est un service de messagerie temporaire. Nous l'avons créé pour que vous puissiez faire le travail et vous débarrasser des e-mails indésirables avec des adresses e-mail supprimées à la fin d'un certain temps.";
						$lang["10mails_is_2"] = "Nous avons créé <b>" . $tma_name . "</b> adresse pour vous. Personne ne l'a utilisé avant vous, et nous ne le donnerons à personne après vous. Par défaut, nous accordons à chaque adresse électronique 10 minutes. Vous pouvez prolonger le temps d'une heure en cliquant sur le bouton \"<span class=\"giveme60btn\">.</span>\" si vous le souhaitez. Nous supprimons définitivement les adresses e-mail expirées avec les e-mails.";
						$lang["where_can_i_use"] = "Où puis- je l' utiliser?";
						$lang["you_can_use"] = "Peut-être pour recevoir un email de confirmation d'un site dont vous êtes membre ou pour ouvrir plusieurs abonnements sur le même site. Avec cette méthode, vous ne devez pas toujours ouvrir une nouvelle adresse e-mail et vous gagnez du temps.";
						$lang["got_it"] = "Comprendre";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Aimez-vous notre application?</b> Informez également vos amis 😊";
						$lang["share_txt_nourl"] = "Obtenez%20un%20nouveau%20compte%20de%20messagerie%20avec%2010%20minutes%20de%20messagerie%20temporaire%20et%20gardez%20votre%20confidentialit%C3%A9%20en%20ligne.%20Et%20c%27est%20totalement%20gratuit%21%0AEssayez-le%20%C3%A0%20l%27adresse";

					//Notifications
						$lang["error"] = "Erreur.";
						$lang["time_extended"] = "Temps prolongé de 1 heure.";
						$lang["time_extended_err"] = "Le temps ne s'est pas prolongé. Veuillez réessayer.";

			}







	//ITALIAN


			else if ($_COOKIE["LANG"] == "it") {

				//Head TAG
						$lang["title_tag"] = "E-mail temporanea di 10 minuti - 10mails.net";
						$lang["meta_description"] = "Ottieni un nuovo account e-mail con 10 minuti di mail temporanea e mantieni la tua privacy online.";

				//MAIN

					//Container
						$lang["language"] = "Italiano";
						$lang["language_code"] = "It";

					//Body
						$lang["welcome"] = "Benvenuto!";
						$lang["this_is_your"] = "Questo è il tuo indirizzo email temporaneo.";
						$lang["signup_with_this"] = "Iscriviti con questa email a:";
						$lang["inbox"] = "Posta in arrivo";
						$lang["total_mail"] = "Hai <b></b> email in totale.";
						$lang["no_email"] = "Non hai ancora nessuna email.";

					//SEO
						$lang["seo_what_is"] = "Che cos'è E-mail Temporanea di 10 Minuti?";
						$lang["seo_p1"] = "Indirizzi di posta elettronica usa e getta che si autodistruggono dopo un certo periodo di tempo. Non è necessario registrarsi e accedere per utilizzare questo servizio. È anche noto come <b>posta temporanea</b>, <b>posta 10 minuti</b>, <b>posta elettronica usa e getta</b>, <b>posta guerriglia</b>, <b>posta elettronica usa e getta</b> o <b>posta falsa</b>.";
						$lang["seo_p2"] = "Dovrai fornire il tuo indirizzo e-mail per eseguire molte operazioni su Internet. Per accedere al contenuto dei siti del forum, scaricare un file o utilizzare un servizio, è necessario abbonarsi a quel sito. Questo farà sì che la tua casella di posta sia piena di e-mail spam. La posta elettronica temporanea usa e getta è il modo migliore per proteggere dalla posta spam.";
						$lang["seo_safe"] = "Garantire";
						$lang["seo_safe_p"] = "Gli indirizzi e-mail sono esclusivamente monouso e non sono accessibili a nessuno diverso da te.";
						$lang["seo_fast"] = "Veloce";
						$lang["seo_fast_p"] = "Le e-mail verranno inviate nella posta in arrivo il più presto possibile. Quindi non devi aspettare.";
						$lang["seo_ex_time"] = "Tempo Allungabile";
						$lang["seo_ex_time_p"] = "Se dici che 10 minuti non sono sufficienti, puoi prolungare il tempo di un'ora.";

					//Footer
						$lang["about"] = "Di";
						$lang["privacy"] = "Vita privata";
						$lang["terms"] = "Condizioni";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> email create.";

				//TOOLTIPS

						$lang["what_is_this"] = "Cos'è questo?";
						$lang["copy"] = "Copia";
						$lang["copied"] = "Copiato!";
						$lang["extend_time"] = "Allungare il tempo";
						$lang["refresh"] = "Ricaricare";

				//MODALS

					//Languages
						$lang["languages"] = "Le lingue";
						$lang["italian"] = "<b>Italiano</b>";

					//About
						$lang["whats_10mails"] = "Che cos'è 10mails.net?";
						$lang["10mails_is"] = "10mails.net è un servizio email temporaneo. L'abbiamo creato per farti svolgere il lavoro e sbarazzarci di email spazzatura con indirizzi email cancellati alla fine di un certo periodo di tempo.";
						$lang["10mails_is_2"] = "Noi 'hai creato <b>" . $tma_name . "</b> indirizzo per te. Nessuno l'ha usato prima di te, e non lo daremo a nessuno dopo di te. Per impostazione predefinita, diamo ad ogni indirizzo email 10 minuti. È possibile prolungare il tempo di 1 ora facendo clic sul pulsante \"<span class=\"giveme60btn\">.</span>\", se lo si desidera. Eliminiamo definitivamente gli indirizzi email scaduti con e-mail.";
						$lang["where_can_i_use"] = "Dove posso usarlo?";
						$lang["you_can_use"] = "Forse per ricevere un'email di conferma da un sito di cui sei membro o per aprire più iscrizioni sullo stesso sito. Con questo metodo, non è sempre necessario aprire un nuovo indirizzo e-mail e risparmiare tempo.";
						$lang["got_it"] = "Capire";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Ti piace la nostra app?</b> Fai sapere anche ai tuoi amici 😊";
						$lang["share_txt_nourl"] = "Ottieni%20un%20nuovo%20account%20e-mail%20con%2010%20minuti%20di%20e-mail%20temporanea%20e%20mantieni%20la%20tua%20privacy%20online.%20Ed%20%C3%A8%20completamente%20gratuito%21%0AProvalo%20su";

					//Notifacations
						$lang["error"] = "Errore.";
						$lang["time_extended"] = "Tempo esteso di 1 ora.";
						$lang["time_extended_err"] = "Il tempo non è esteso. Per favore riprova.";

			}







	//DUTCH


			else if ($_COOKIE["LANG"] == "nl") {

				//Head TAG
						$lang["title_tag"] = "Tijdelijke e-mail van 10 minuten - 10mails.net";
						$lang["meta_description"] = "Ontvang een nieuw e-mailaccount met 10 minuten tijdelijke mail en houd uw privacy online.n";

				//MAIN

					//Container
						$lang["language"] = "Nederlands";
						$lang["language_code"] = "Ne";

					//Body
						$lang["welcome"] = "Welkom!";
						$lang["this_is_your"] = "Dit is uw tijdelijke e-mailadres.";
						$lang["signup_with_this"] = "Meld u aan met deze e-mail aan:";
						$lang["inbox"] = "Postvak IN";

						if ($mail_count > 1)
							$lang["total_mail"] = "Je hebt in totaal <b></b> e-mails.";
						else
							$lang["total_mail"] = "Je hebt in totaal <b></b> e-mail.";

						$lang["no_email"] = "Je hebt nog geen e-mail.";

					//SEO
						$lang["seo_what_is"] = "Wat is Tijdelijke E-mail van 10 Minuten?";
						$lang["seo_p1"] = "Wegwerp e-mailadressen die zichzelf na een bepaalde periode vernietigen. U hoeft zich niet te registreren en in te loggen om deze service te gebruiken. Het is ook bekend als <b>tijdelijke post</b>, <b>post van 10 minuten</b>, <b>wegwerp-e-mail</b>, <b>guerrilla-mail</b>, <b>wegwerp-e-mail</b> of <b>nepmail</b>.";
						$lang["seo_p2"] = "U moet uw e-mailadres opgeven om veel bewerkingen op internet uit te voeren. Om toegang te krijgen tot inhoud op forumsites, een bestand te downloaden of een service te gebruiken, moet u zich op die site abonneren. Hierdoor wordt uw inbox gevuld met spam-e-mails. Tijdelijke wegwerp-e-mail is de beste manier om u te beschermen tegen spam-mails.";
						$lang["seo_safe"] = "Beveiligen";
						$lang["seo_safe_p"] = "E-mailadressen zijn uitsluitend voor eenmalig gebruik en kunnen door niemand anders worden geopend dan jij.";
						$lang["seo_fast"] = "Snel";
						$lang["seo_fast_p"] = "E-mails worden zo snel mogelijk in uw inbox geplaatst. U hoeft dus niet te wachten.";
						$lang["seo_ex_time"] = "Uitschuifbare Tijd";
						$lang["seo_ex_time_p"] = "Als je zegt dat 10 minuten niet genoeg is, kun je de tijd met een uur verlengen.";

					//Footer
						$lang["about"] = "Wat betreft";
						$lang["privacy"] = "Privacy";
						$lang["terms"] = "Voorwaarden";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-mails aangemaakt.";

				//TOOLTIPS

						$lang["what_is_this"] = "Wat is dit?";
						$lang["copy"] = "Kopiëren";
						$lang["copied"] = "Gekopieerde!";
						$lang["extend_time"] = "Verleng de tijd";
						$lang["refresh"] = "Verversen";

				//MODALS

					//Languages
						$lang["languages"] = "Talen";
						$lang["dutch"] = "<b>Nederlands</b>";

					//About
						$lang["whats_10mails"] = "Wat is 10mails.net?";
						$lang["10mails_is"] = "10mails.net is een tijdelijke e-mailservice. We hebben het gemaakt zodat u de klus kunt klaren en ongewenste e-mailadressen met verwijderde e-mailadressen aan het einde van een bepaalde periode kunt verwijderen.";
						$lang["10mails_is_2"] = "We hebben gemaakt <b>" . $tma_name . "</b> adres voor u. Niemand heeft het voor je gebruikt en we zullen het niemand na jou geven. Standaard geven we elk e-mailadres 10 minuten. U kunt de tijd verlengen met 1 uur door op de knop \"<span class=\"giveme60btn\">.</span>\" te klikken, als u dat wilt. We verwijderen verlopen e-mailadressen permanent met e-mails.";
						$lang["where_can_i_use"] = "Waar kan ik het gebruiken?";
						$lang["you_can_use"] = "Misschien om een ​​bevestigingsmail te ontvangen van een site waar u lid van bent of om meerdere lidmaatschappen op dezelfde site te openen. Met deze methode hoeft u niet altijd een nieuw e-mailadres te openen en bespaart u tijd.";
						$lang["got_it"] = "Begrijpen";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Vind je onze app leuk?</b> Laat je vrienden het ook weten 😊";
						$lang["share_txt_nourl"] = "Ontvang%20een%20nieuw%20e-mailaccount%20met%2010%20minuten%20tijdelijke%20e-mail%20en%20houd%20uw%20privacy%20online.%20En%20het%20is%20helemaal%20gratis%21%0AProbeer%20het%20op";

					//Notifications
						$lang["error"] = "Fout.";
						$lang["time_extended"] = "Tijd verlengd met 1 uur.";
						$lang["time_extended_err"] = "De tijd wordt niet verlengd. Probeer het opnieuw.";

			}







	//NORWEGIAN


			else if ($_COOKIE["LANG"] == "no") {

				//Head TAG
						$lang["title_tag"] = "10 Minutters Mail - 10mails.net";
						$lang["meta_description"] = "Få en ny e-postkonto med 10 minutter med midlertidig mail, og hold privatlivet ditt på nettet.";

				//MAIN

					//Container
						$lang["language"] = "Norsk";
						$lang["language_code"] = "No";

					//Body
						$lang["welcome"] = "Velkommen!";
						$lang["this_is_your"] = "Dette er din midlertidige e-postadresse.";
						$lang["signup_with_this"] = "Registrer deg med denne e-posten til:";
						$lang["inbox"] = "Innboks";

						if ($mail_count > 1)
							$lang["total_mail"] = "Du har totalt <b></b> e-postmeldinger.";
						else
							$lang["total_mail"] = "Du har totalt <b></b> e-post.";

						$lang["no_email"] = "Du har ingen e-post ennå.";

					//SEO
						$lang["seo_what_is"] = "Hva er Midlertidig E-post på 10 Minutter?";
						$lang["seo_p1"] = "Engangs-e-postadresser som selvdestruererer etter en viss periode. Du trenger ikke registrere deg og logge inn for å bruke denne tjenesten. Det er også kjent som <b>temp-post</b>, <b>10 minutters post</b>, <b>engangs-e-post</b>, <b>geriljapost</b>, <b>kast-e-post</b> eller <b>falsk post</b>.";
						$lang["seo_p2"] = "Du må oppgi e-postadressen din for å utføre mange operasjoner på Internett. For å få tilgang til innhold på forumsider, laste ned en fil eller bruke en tjeneste, må du abonnere på dette nettstedet. Dette vil føre til at innboksen din blir fylt med spam-e-poster. Midlertidig engangs-e-post er den beste måten å beskytte mot spam-post.";
						$lang["seo_safe"] = "Sikre";
						$lang["seo_safe_p"] = "E-postadresser er kun til engangsbruk og kan ikke nås av andre enn deg.";
						$lang["seo_fast"] = "Rask";
						$lang["seo_fast_p"] = "E-post slipper inn i innboksen din så snart som mulig. Så du trenger ikke å vente.";
						$lang["seo_ex_time"] = "Forlengbar Tid";
						$lang["seo_ex_time_p"] = "Hvis du sier at 10 minutter ikke er nok, kan du forlenge tiden med en time.";

					//Footer
						$lang["about"] = "Handle om";
						$lang["privacy"] = "Personvern";
						$lang["terms"] = "Vilkår";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-poster opprettet.";

				//TOOLTIPS

						$lang["what_is_this"] = "Hva er dette?";
						$lang["copy"] = "Kopiere";
						$lang["copied"] = "Kopiert!";
						$lang["extend_time"] = "Forlenge tiden";
						$lang["refresh"] = "Forfriske";

				//MODALS

					//Languages
						$lang["languages"] = "Språk";
						$lang["norwegian"] = "<b>Norsk</b>";

					//About
						$lang["whats_10mails"] = "Hva er 10mails.net?";
						$lang["10mails_is"] = "10mails.net er en midlertidig e-posttjeneste. Vi opprettet det for at du får jobben gjort og bli kvitt søppelpost med slettede e-postadresser på slutten av en bestemt periode.";
						$lang["10mails_is_2"] = "Vi har opprettet <b>" . $tma_name . "</b> adresse for deg. Ingen brukte det før deg, og vi vil ikke gi det til noen etter deg. Som standard gir vi hver e-postadresse 10 minutter. Du kan forlenge tiden i 1 time ved å klikke på \"<span class=\"giveme60btn\">.</span>\" knappen hvis du vil. Vi sletter fortløpende e-postadresser permanent med e-post.";
						$lang["where_can_i_use"] = "Hvor kan jeg bruke den?";
						$lang["you_can_use"] = "Kanskje å motta en e-postbekreftelse fra et nettsted du er medlem av eller for å åpne flere medlemskap på samme nettsted. Med denne metoden trenger du ikke alltid å åpne en ny e-postadresse, og du sparer tid.";
						$lang["got_it"] = "Forstå";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Liker du appen vår?</b> La vennene dine også vite det 😊";
						$lang["share_txt_nourl"] = "F%C3%A5%20en%20ny%20e-postkonto%20med%2010%20minutter%20med%20midlertidig%20e-post%2C%20og%20hold%20privatlivet%20ditt%20p%C3%A5%20nettet.%20Og%20det%20er%20helt%20gratis%21%0APr%C3%B8v%20det%20p%C3%A5";

					//Notifications
						$lang["error"] = "Feil.";
						$lang["time_extended"] = "Tid forlenget med 1 time.";
						$lang["time_extended_err"] = "Tiden forlenges ikke. Vær så snill, prøv på nytt.";

			}







	//PORTUGUESE


			else if ($_COOKIE["LANG"] == "pt") {

				//Head TAG
						$lang["title_tag"] = "Email por 10 Minutos - 10mails.net";
						$lang["meta_description"] = "Obtenha uma nova conta de e-mail sem ter que se inscrever por 10 minutos de mail temporário e manter a privacidade na Internet.";

				//MAIN

					//Container
						$lang["language"] = "Português";
						$lang["language_code"] = "Pt";

					//Body
						$lang["welcome"] = "Bem vinda!";
						$lang["this_is_your"] = "Este é o seu endereço de email temporário.";
						$lang["signup_with_this"] = "Inscreva-se com este email para:";
						$lang["inbox"] = "Caixa de entrada";

						if ($mail_count > 1)
							$lang["total_mail"] = "Você tem <b></b> e-mails no total.";
						else
							$lang["total_mail"] = "Você tem <b></b> e-mail no total.";

						$lang["no_email"] = "Você ainda não tem email.";

					//SEO
						$lang["seo_what_is"] = "O que é um Email Temporário de 10 Minutos?";
						$lang["seo_p1"] = "Endereços de e-mail descartáveis que se auto-destruem após um determinado período de tempo. Você não precisa se registrar e fazer login para usar este serviço. É também conhecido como <b>mail temporário</b>, <b>email de 10 minutos</b>, <b>e-mail descartável</b>, <b>guerrilla mail</b>, <b>e-mail descartável</b> ou <b>correio falso</b>.";
						$lang["seo_p2"] = "Você precisará fornecer seu endereço de e-mail para realizar muitas operações na Internet. Para acessar o conteúdo em sites do fórum, baixar um arquivo ou usar um serviço, você terá que se inscrever nesse site. Isso fará com que sua caixa de entrada seja preenchida com e-mails de spam. O e-mail descartável temporário é a melhor maneira de se proteger contra e-mails de spam.";
						$lang["seo_safe"] = "Proteger";
						$lang["seo_safe_p"] = "Endereços de e-mail são para uso único e não podem ser acessados por ninguém além de você.";
						$lang["seo_fast"] = "Rápido";
						$lang["seo_fast_p"] = "Os e-mails serão enviados à sua caixa de entrada o mais rápido possível. Então você não precisa esperar.";
						$lang["seo_ex_time"] = "Tempo Extensível";
						$lang["seo_ex_time_p"] = "Se você disser que 10 minutos não são suficientes, você pode prolongar o tempo em uma hora.";

					//Footer
						$lang["about"] = "Sobre";
						$lang["privacy"] = "Privacidade";
						$lang["terms"] = "Termos";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-mails criados.";

				//TOOLTIPS

						$lang["what_is_this"] = "O que é isso?";
						$lang["copy"] = "Cópia de";
						$lang["copied"] = "Copiado!";
						$lang["extend_time"] = "Estender o prazo";
						$lang["refresh"] = "Atualizar";

				//MODALS

					//Languages
						$lang["languages"] = "Línguas";
						$lang["portuguese"] = "<b>Português</b>";

					//About
						$lang["whats_10mails"] = "O que é um arquivo 10mails.net?";
						$lang["10mails_is"] = "10mails.net é um serviço de email temporário. Nós criamos para você fazer o trabalho e se livrar de e-mails indesejados com endereços de e-mail excluídos no final de um determinado período de tempo.";
						$lang["10mails_is_2"] = "Temos criado <b>" . $tma_name . "</b> endereço para você. Ninguém usou antes de você, e não vamos dar a ninguém depois de você. Por padrão, damos a cada endereço de email 10 minutos. Você pode prolongar o tempo por 1 hora, clicando no botão \"<span class=\"giveme60btn\">.</span>\", se quiser. Nós excluímos permanentemente endereços de e-mail expirados com e-mails.";
						$lang["where_can_i_use"] = "Onde posso usá-lo?";
						$lang["you_can_use"] = "Talvez para receber um email de confirmação de um site do qual você é membro ou para abrir várias associações no mesmo site. Com esse método, você nem sempre precisa abrir um novo endereço de e-mail e economizar tempo.";
						$lang["got_it"] = "Compreendo";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Você gosta do nosso aplicativo?</b> Deixe seus amigos também saberem 😊";
						$lang["share_txt_nourl"] = "Obtenha%20uma%20nova%20conta%20de%20e-mail%20com%2010%20minutos%20de%20e-mail%20tempor%C3%A1rio%20e%20mantenha%20sua%20privacidade%20on-line.%20E%20%C3%A9%20completamente%20gr%C3%A1tis%21%0AExperimente%20agora";

					//Notifications
						$lang["error"] = "Erro.";
						$lang["time_extended"] = "Tempo prorrogado por 1 hora.";
						$lang["time_extended_err"] = "O tempo não se estende. Por favor, tente novamente.";

			}







	//SWEDISH


			else if ($_COOKIE["LANG"] == "sv") {

				//Head TAG
						$lang["title_tag"] = "10-minuters tillfällig e-post - 10mails.net";
						$lang["meta_description"] = "Få ett nytt e-postkonto med 10 minuter med tillfälligt mail och behåll din integritet online.";

				//MAIN

					//Container
						$lang["language"] = "Svenska";
						$lang["language_code"] = "Sv";

					//Body
						$lang["welcome"] = "Välkommen!";
						$lang["this_is_your"] = "Det här är din tillfälliga e-postadress.";
						$lang["signup_with_this"] = "Registrera med detta email till:";
						$lang["inbox"] = "Inkorg";

						if ($mail_count > 1)
							$lang["total_mail"] = "Du har totalt <b></b> e-postmeddelanden.";
						else
							$lang["total_mail"] = "Du har totalt <b></b> email.";

						$lang["no_email"] = "Du har inget email ännu.";

					//SEO
						$lang["seo_what_is"] = "Vad är 10-Minuters Tillfällig E-post?";
						$lang["seo_p1"] = "Engångs-e-postadresser som självförstörs efter en viss tid. Du behöver inte registrera dig och logga in för att använda den här tjänsten. Det är också känt som <b>temp-post</b>, <b>10 minuters mail</b>, <b>engångs-e-post</b>, <b>gerillapost</b>, <b>kastad e-post</b> eller <b>falsk post</b>.";
						$lang["seo_p2"] = "Du måste ange din e-postadress för att utföra många åtgärder på Internet. För att få åtkomst till innehåll på forumwebbplatser, ladda ner en fil eller använda en tjänst måste du prenumerera på den webbplatsen. Detta kommer att göra att din inkorg fylls med skräppostmeddelanden. Tillfällig engångs-e-post är det bästa sättet att skydda mot skräppost.";
						$lang["seo_safe"] = "Säkra";
						$lang["seo_safe_p"] = "E-postadresser är endast för engångsbruk och kan inte nås av någon annan än dig.";
						$lang["seo_fast"] = "Snabb";
						$lang["seo_fast_p"] = "E-post kommer att släppas i din inkorg så snart som möjligt. Så du behöver inte vänta.";
						$lang["seo_ex_time"] = "Förlängbar Tid";
						$lang["seo_ex_time_p"] = "Om du säger att 10 minuter inte räcker kan du förlänga tiden med en timme.";

					//Footer
						$lang["about"] = "Handla om";
						$lang["privacy"] = "Integritet";
						$lang["terms"] = "Villkor";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-postmeddelanden skapade.";

				//TOOLTIPS

						$lang["what_is_this"] = "Vad är detta?";
						$lang["copy"] = "Kopia";
						$lang["copied"] = "Kopieras!";
						$lang["extend_time"] = "Förlänga tiden";
						$lang["refresh"] = "Uppdatera";

				//MODALS

					//Languages
						$lang["languages"] = "Språk";
						$lang["swedish"] = "<b>Svenska</b>";

					//About
						$lang["whats_10mails"] = "Vad är 10mails.net?";
						$lang["10mails_is"] = "10mails.net är en tillfällig e-posttjänst. Vi skapade det för att du får jobbet gjort och bli av med skräppost med borttagna e-postadresser i slutet av en viss tid.";
						$lang["10mails_is_2"] = "Vi har skapat <b>" . $tma_name . "</b> adress för dig. Ingen använde det för dig, och vi kommer inte ge det till någon efter dig. Som standard ger vi varje e-postadress 10 minuter. Du kan förlänga tiden i 1 timme genom att klicka på \"<span class=\"giveme60btn\">.</span>\" knappen om du vill. Vi tar bort utgående e-postadresser permanent med e-post.";
						$lang["where_can_i_use"] = "Var kan jag använda den?";
						$lang["you_can_use"] = "Kanske att få ett bekräftelsemail från en webbplats du är medlem i eller att öppna flera medlemskap på samma sajt. Med den här metoden behöver du inte alltid öppna en ny e-postadress och du sparar tid.";
						$lang["got_it"] = "Förstå";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Gillar du vår app?</b> Låt dina vänner också veta 😊";
						$lang["share_txt_nourl"] = "F%C3%A5%20ett%20nytt%20e-postkonto%20med%2010%20minuter%20med%20tillf%C3%A4lligt%20e-post%20och%20beh%C3%A5ll%20din%20integritet%20online.%20Och%20det%20%C3%A4r%20helt%20gratis%21%0AProva%20nu";

					//Notifications
						$lang["error"] = "Fel.";
						$lang["time_extended"] = "Tid förlängd med 1 timme.";
						$lang["time_extended_err"] = "Tiden utökas inte. Var god försök igen.";

			}







	//TURKISH


			else if ($_COOKIE["LANG"] == "tr") {

				//Head TAG
						$lang["title_tag"] = "10 Dakikalık Geçici E-posta - 10mails.net";
						$lang["meta_description"] = "10 dakikalık geçici mail ile üye olmaya gerek kalmadan yeni bir e-posta hesabı edin ve internette gizliliğini koru.";

				//MAIN

					//Container
						$lang["language"] = "Türkçe";
						$lang["language_code"] = "Tr";

					//Body
						$lang["welcome"] = "Hoşgeldin!";
						$lang["this_is_your"] = "Bu senin geçici e-posta adresin.";
						$lang["signup_with_this"] = "Bu e-posta ile şuraya kaydol:";
						$lang["inbox"] = "Gelen Kutusu";
						$lang["total_mail"] = "Toplam <b></b> e-postan var.";
						$lang["no_email"] = "Henüz e-postan yok.";

					//SEO
						$lang["seo_what_is"] = "10 Dakikalık Geçici E-posta Nedir?";
						$lang["seo_p1"] = "Belirli bir süre geçtikten sonra kendini imha eden tek kullanımlık e-posta adresleridir. Bu servisi kullanmak için kaydolmanıza ve giriş yapmanıza gerek yoktur. <b>Temp mail</b>, <b>10 minute mail</b>, <b>disposable email</b>, <b>guerrilla mail</b>, <b>throwaway mail</b> veya <b>sahte mail</b> gibi isimlerle de bilinir.";
						$lang["seo_p2"] = "İnternet üzerindeki birçok işlemi gerçekleştirebilmek için e-posta adresinizi vermeniz gerekir. Forum sitelerinde içeriğe ulaşmak, bir dosyayı indirmek ya da bir servisi kullanmak için o siteye üye olmak zorunda kalırsınız. Bu da gelen kutunuzun spam maillerle dolmasına neden olur. Tek kullanımlık geçici e-posta spam maillerden korunmanın en iyi yoludur.";
						$lang["seo_safe"] = "Güvenli";
						$lang["seo_safe_p"] = "E-posta adresleri tek kullanımlıktır ve sizden başkası erişemez.";
						$lang["seo_fast"] = "Hızlı";
						$lang["seo_fast_p"] = "E-postalar en kısa sürede gelen kutunuza düşer. Böylece beklemek zorunda kalmazsınız.";
						$lang["seo_ex_time"] = "Uzatılabilir Süre";
						$lang["seo_ex_time_p"] = "10 dakika bana yetmez diyorsanız süreyi bir saat uzatabilirsiniz.";

					//Footer
						$lang["about"] = "Hakkında";
						$lang["privacy"] = "Gizlilik";
						$lang["terms"] = "Şartlar";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> e-posta oluşturuldu.";

				//TOOLTIPS

						$lang["what_is_this"] = "Bu nedir?";
						$lang["copy"] = "Kopyala";
						$lang["copied"] = "Kopyalandı!";
						$lang["extend_time"] = "Süreyi uzat";
						$lang["refresh"] = "Yenile";

				//MODALS

					//Languages
						$lang["languages"] = "Diller";
						$lang["turkish"] = "<b>Türkçe</b>";

					//About
						$lang["whats_10mails"] = "10mails.net nedir?";
						$lang["10mails_is"] = "10mails.net geçici e-posta servisidir. Belirli süre sonunda silinen e-posta adresleriyle internette anonim kalman ve gereksiz e-postalardan kurtulman için ürettik.";
						$lang["10mails_is_2"] = "<b>" . $tma_name . "</b> adresini sana özel oluşturduk. Bunu senden önce kimse kullanmadı ve senden sonra da kimseye vermeyeceğiz. Her e-posta adresine varsayılan olarak 10 dakika süre veririz. İstersen \"<span class=\"giveme60btn\">.</span>\" butonuna tıklayarak süreyi 1 saat uzatabilirsin. Süresi biten e-posta adreslerini içindeki e-postalarla birlikte kalıcı olarak sileriz.";
						$lang["where_can_i_use"] = "Nerelerde kullanabilirim?";
						$lang["you_can_use"] = "Belki de üye olduğun bir siteden onay e-postası alabilmek ya da aynı sitede birden çok üyelik açabilmek için. Kişisel e-posta adresini kullanmak istemediğin her yerde kullanabilirsin. Bu yöntemle her defasında yeni bir e-posta adresi açmakla uğraşmaz ve zamandan kazanırsın.";
						$lang["got_it"] = "Anladım";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Uygulamamızı beğendin mi?</b> Arkadaşlarına öner 😊";
						$lang["share_txt_nourl"] = "10%20dakikal%C4%B1k%20ge%C3%A7ici%20e-posta%20ile%20%C3%BCye%20olmaya%20gerek%20kalmadan%20yeni%20bir%20e-posta%20hesab%C4%B1%20edin%20ve%20internette%20gizlili%C4%9Fini%20koru.%20%C3%9Cstelik%20tamamen%20%C3%BCcretsiz%21%0AHemen%20dene";

					//Notifications
						$lang["error"] = "Hata.";
						$lang["time_extended"] = "Süre 1 saat uzatıldı.";
						$lang["time_extended_err"] = "Süre uzatılamadı. Lütfen tekrar deneyin.";

			}






	//RUSSIAN


			else if ($_COOKIE["LANG"] == "ru") {

				//Head TAG
						$lang["title_tag"] = "10-минутная временная электронная почта - 10mails.net";
						$lang["meta_description"] = "Получите новую учетную запись электронной почты с 10-минутным временным письмом и сохраните свою конфиденциальность в Интернете.";

				//MAIN

					//Container
						$lang["language"] = "Русский";
						$lang["language_code"] = "Ru";

					//Body
						$lang["welcome"] = "Добро пожаловать!";
						$lang["this_is_your"] = "Это ваш временный адрес электронной почты.";
						$lang["signup_with_this"] = "Зарегистрируйтесь по этому адресу:";
						$lang["inbox"] = "входящие";

						if ($mail_count > 1)
							$lang["total_mail"] = "Всего у вас <b></b> письма.";
						else
							$lang["total_mail"] = "Всего у вас <b></b> электронное письмо.";

						$lang["no_email"] = "У вас еще нет электронной почты.";

					//SEO
						$lang["seo_what_is"] = "Что такое 10-минутная временная электронная почта?";
						$lang["seo_p1"] = "Это одноразовые адреса электронной почты, которые самоуничтожаются после определенного периода времени. Вам не нужно регистрироваться и входить в систему, чтобы использовать эту услугу. Temp mail также известен такими именами, как 10 minute mail, disposable email, guerrilla mail, throwaway e-mail или поддельная почта.";
						$lang["seo_p2"] = "Вам нужно указать свой адрес электронной почты, чтобы иметь возможность выполнять множество транзакций в интернете. Вы должны зарегистрироваться на этом сайте, чтобы получить доступ к контенту на сайтах форумов, загрузить файл или воспользоваться услугой. Это приведет к тому, что ваш почтовый ящик будет заполнен спам-письмами. Одноразовая временная электронная почта-лучший способ защитить от спама.";
						$lang["seo_safe"] = "Безопасный";
						$lang["seo_safe_p"] = "Адреса электронной почты являются одноразовыми и доступны только вам.";
						$lang["seo_fast"] = "Быстрый";
						$lang["seo_fast_p"] = "Письма попадают в ваш почтовый ящик как можно скорее. Таким образом, вам не придется ждать.";
						$lang["seo_ex_time"] = "Расширяемый Период";
						$lang["seo_ex_time_p"] = "Если вы говорите, что мне не хватает 10 минут, вы можете продлить время на один час.";

					//Footer
						$lang["about"] = "Около";
						$lang["privacy"] = "Конфиденциальность";
						$lang["terms"] = "термины";
						$lang["emails_created"] = "Создано <b>" . $created_mail_count . "</b> электронных писем.";

				//TOOLTIPS

						$lang["what_is_this"] = "Что это?";
						$lang["copy"] = "копия";
						$lang["copied"] = "Скопировано!";
						$lang["extend_time"] = "Продлить время";
						$lang["refresh"] = "обновление";

				//MODALS

					//Languages
						$lang["languages"] = "Языки";
						$lang["russian"] = "<b>Русский</b>";

					//About
						$lang["whats_10mails"] = "Что такое 10mails.net?";
						$lang["10mails_is"] = "10mails.net это временный почтовый сервис. Мы создали его для того, чтобы вы выполнили свою работу и избавились от нежелательных писем с удаленными адресами электронной почты в конце определенного периода времени.";
						$lang["10mails_is_2"] = "У нас создан <b>" . $tma_name . "</b> адрес для вас. Никто не использовал его до вас, и мы не дадим его никому после вас. По умолчанию мы даем каждому адресу электронной почты 10 минут. Вы можете продлить время на 1 час, нажав кнопку \"<span class=\"giveme60btn\">.</span>\", если хотите. Мы навсегда удаляем адреса электронной почты с истекшим сроком действия.";
						$lang["where_can_i_use"] = "Где я могу использовать это?";
						$lang["you_can_use"] = "Может быть, получить подтверждение по электронной почте с сайта, членом которого вы являетесь, или открыть несколько членств на одном сайте. При использовании этого метода вам не всегда нужно открывать новый адрес электронной почты, и вы экономите время.";
						$lang["got_it"] = "Понимаю";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Вам нравится наше приложение?</b> Пусть ваши друзья тоже знают 😊";
						$lang["share_txt_nourl"] = "%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D0%B5%20%D0%BD%D0%BE%D0%B2%D1%83%D1%8E%20%D1%83%D1%87%D0%B5%D1%82%D0%BD%D1%83%D1%8E%20%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C%20%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%BD%D0%BE%D0%B9%20%D0%BF%D0%BE%D1%87%D1%82%D1%8B%20%D1%81%2010-%D0%BC%D0%B8%D0%BD%D1%83%D1%82%D0%BD%D1%8B%D0%BC%20%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%BD%D1%8B%D0%BC%20%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE%D0%BC%20%D0%B8%20%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B8%D1%82%D0%B5%20%D1%81%D0%B2%D0%BE%D1%8E%20%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D1%8C%20%D0%B2%20%D0%98%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82%D0%B5.%20%D0%98%20%D1%8D%D1%82%D0%BE%20%D1%81%D0%BE%D0%B2%D0%B5%D1%80%D1%88%D0%B5%D0%BD%D0%BD%D0%BE%20%D0%B1%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%BE%21%0A%D0%9F%D0%BE%D0%BF%D1%80%D0%BE%D0%B1%D1%83%D0%B9%D1%82%D0%B5%20%D1%81%D0%B5%D0%B9%D1%87%D0%B0%D1%81";

					//Notifications
						$lang["error"] = "Ошибка.";
						$lang["time_extended"] = "Время продлено на 1 час.";
						$lang["time_extended_err"] = "Время не продлено. Пожалуйста, попробуйте еще раз.";

			}







	//UKRANIAN


			else if ($_COOKIE["LANG"] == "uk") {

				//Head TAG
						$lang["title_tag"] = "10-хвилинна тимчасова електронна пошта - 10mails.net";
						$lang["meta_description"] = "Отримайте новий обліковий запис електронної пошти з 10 хвилинами тимчасової електронної пошти та збережіть вашу конфіденційність в Інтернеті.";

				//MAIN

					//Container
						$lang["language"] = "Українська";
						$lang["language_code"] = "Uk";

					//Body
						$lang["welcome"] = "Ласкаво просимо!";
						$lang["this_is_your"] = "Це ваша тимчасова адреса електронної пошти.";
						$lang["signup_with_this"] = "Зареєструйтеся за допомогою цього електронного листа на адресу:";
						$lang["inbox"] = "Вхідні";

						if ($mail_count > 1)
							$lang["total_mail"] = "Загалом у вас є <b></b> листи.";
						else
							$lang["total_mail"] = "Загалом у вас є <b></b> електронна пошта.";

						$lang["no_email"] = "Ви ще не маєте електронної пошти.";

					//SEO
						$lang["seo_what_is"] = "Що таке 10-хвилинна тимчасова електронна пошта?";
						$lang["seo_p1"] = "Це одноразові адреси електронної пошти, які самознищуються після певного періоду часу. Для використання цієї послуги вам не потрібно реєструватися та входити в систему. Temp mail також відомий такими іменами, як 10 minute mail, disposable email, guerrilla mail, throwaway e-mail або підроблена пошта.";
						$lang["seo_p2"] = "Вам потрібно вказати свою адресу електронної пошти, щоб мати можливість виконувати безліч транзакцій в інтернеті. Ви повинні зареєструватися на сайті, щоб отримати доступ до контенту на сайтах форумів, завантажити файл або скористатися послугою. Це призведе до того, що Ваша поштова скринька буде заповнена спам-листами. Одноразова тимчасова електронна пошта-найкращий спосіб захистити від спаму.";
						$lang["seo_safe"] = "Безпечний";
						$lang["seo_safe_p"] = "Адреси електронної пошти є одноразовими і доступні тільки вам.";
						$lang["seo_fast"] = "Швидкий";
						$lang["seo_fast_p"] = "Листи потрапляють у Вашу поштову скриньку якомога швидше. Таким чином, вам не доведеться чекати.";
						$lang["seo_ex_time"] = "Розширюваний Період";
						$lang["seo_ex_time_p"] = "Якщо ви говорите, що мені не вистачає 10 хвилин, ви можете продовжити час на одну годину.";

					//Footer
						$lang["about"] = "Про";
						$lang["privacy"] = "Конфіденційність";
						$lang["terms"] = "Умови";
						$lang["emails_created"] = "Створено <b>" . $created_mail_count . "</b> листів.";

				//TOOLTIPS

						$lang["what_is_this"] = "Що це?";
						$lang["copy"] = "Копіювати";
						$lang["copied"] = "Скопійовано!";
						$lang["extend_time"] = "Подовжити час";
						$lang["refresh"] = "Оновити";

				//MODALS

					//Languages
						$lang["languages"] = "Мови";
						$lang["ukranian"] = "<b>Українська</b>";

					//About
						$lang["whats_10mails"] = "Що таке 10mails.net?";
						$lang["10mails_is"] = "10mails.net є тимчасовою поштовою службою. Ми створили його для вас, щоб отримати роботу і позбутися від небажаної електронної пошти з видаленими адресами електронної пошти в кінці певного періоду часу.";
						$lang["10mails_is_2"] = "У нас створено <b>" . $tma_name . "</b> адреса для вас. Ніхто не використовував його перед вами, і ми не дамо її нікому після вас. За замовчуванням кожній електронній адресі ми надаємо 10 хвилин. Ви можете продовжити час на 1 годину, натиснувши кнопку \"<span class=\"giveme60btn\">.</span>\", якщо хочете. Ми остаточно видаляємо адреси електронної пошти, термін дії яких закінчився.";
						$lang["where_can_i_use"] = "Де я можу його використовувати?";
						$lang["you_can_use"] = "Можливо, для отримання підтвердження електронної пошти з сайту, до якого ви входите, або для відкриття кількох членств на цьому ж сайті. За допомогою цього методу не завжди потрібно відкривати нову адресу електронної пошти, а також заощаджувати час.";
						$lang["got_it"] = "Зрозумійте";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>Вам подобається наша програма?</b> Нехай ваші друзі також знають 😊";
						$lang["share_txt_nourl"] = "%D0%9E%D1%82%D1%80%D0%B8%D0%BC%D0%B0%D0%B9%D1%82%D0%B5%20%D0%BD%D0%BE%D0%B2%D0%B8%D0%B9%20%D0%BE%D0%B1%D0%BB%D1%96%D0%BA%D0%BE%D0%B2%D0%B8%D0%B9%20%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%20%D0%B5%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%BD%D0%BE%D1%97%20%D0%BF%D0%BE%D1%88%D1%82%D0%B8%20%D0%B7%2010%20%D1%85%D0%B2%D0%B8%D0%BB%D0%B8%D0%BD%D0%B0%D0%BC%D0%B8%20%D1%82%D0%B8%D0%BC%D1%87%D0%B0%D1%81%D0%BE%D0%B2%D0%BE%D1%97%20%D0%B5%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BD%D0%BD%D0%BE%D1%97%20%D0%BF%D0%BE%D1%88%D1%82%D0%B8%20%D1%82%D0%B0%20%D0%B7%D0%B1%D0%B5%D1%80%D0%B5%D0%B6%D1%96%D1%82%D1%8C%20%D0%B2%D0%B0%D1%88%D1%83%20%D0%BA%D0%BE%D0%BD%D1%84%D1%96%D0%B4%D0%B5%D0%BD%D1%86%D1%96%D0%B9%D0%BD%D1%96%D1%81%D1%82%D1%8C%20%D0%B2%20%D0%86%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82%D1%96.%20%D0%86%20%D1%86%D0%B5%20%D0%B0%D0%B1%D1%81%D0%BE%D0%BB%D1%8E%D1%82%D0%BD%D0%BE%20%D0%B1%D0%B5%D0%B7%D0%BA%D0%BE%D1%88%D1%82%D0%BE%D0%B2%D0%BD%D0%BE%21%0A%D0%A1%D0%BF%D1%80%D0%BE%D0%B1%D1%83%D0%B9%D1%82%D0%B5%20%D0%B7%D0%B0%D1%80%D0%B0%D0%B7";

					//Notifications
						$lang["error"] = "Помилка.";
						$lang["time_extended"] = "Час продовжено на 1 годину.";
						$lang["time_extended_err"] = "Час не продовжується. Будь ласка спробуйте ще раз.";

			}







	//HINDU


			else if ($_COOKIE["LANG"] == "hi") {

				//Head TAG
						$lang["title_tag"] = "10-मिनट अस्थायी ई-मेल - 10mails.net";
						$lang["meta_description"] = "10 मिनट के अस्थायी ई-मेल के साथ एक नया ई-मेल खाता प्राप्त करें और अपनी गोपनीयता ऑनलाइन रखें।";

				//MAIN

					//Container
						$lang["language"] = "हिंदू";
						$lang["language_code"] = "Hi";

					//Body
						$lang["welcome"] = "स्वागत हे!";
						$lang["this_is_your"] = "यह आपका अस्थायी ईमेल पता है।";
						$lang["signup_with_this"] = "इस ईमेल के साथ साइन अप करें:";
						$lang["inbox"] = "इनबॉक्स";
						$lang["total_mail"] = "आपके पास कुल <b></b> ईमेल हैं।";
						$lang["no_email"] = "आपके पास अभी तक कोई ईमेल नहीं है";

					//SEO
						$lang["seo_what_is"] = "10 मिनट का अस्थाई ईमेल क्या है?";
						$lang["seo_p1"] = "डिस्पोजेबल ई-मेल पते कि निश्चित समय के बाद आत्म-विनाश। इस सेवा का उपयोग करने के लिए आपको पंजीकरण और लॉगिन करने की आवश्यकता नहीं है। इसे अस्थायी मेल, 10 मिनट मेल, डिस्पोजेबल ईमेल, गुरिल्ला मेल, थ्रोवे ईमेल या नकली मेल के रूप में भी जाना जाता है।";
						$lang["seo_p2"] = "आपको इंटरनेट पर कई ऑपरेशन करने के लिए अपना ई-मेल पता प्रदान करना होगा। फ़ोरम साइटों पर सामग्री तक पहुंचने के लिए, एक फ़ाइल डाउनलोड करें या एक सेवा का उपयोग करें, आपको उस साइट की सदस्यता लेनी होगी। इससे आपका इनबॉक्स स्पैम ईमेल से भर जाएगा। स्पैम मेल से बचाव के लिए अस्थायी डिस्पोजेबल ई-मेल सबसे अच्छा तरीका है।";
						$lang["seo_safe"] = "सुरक्षित";
						$lang["seo_safe_p"] = "ईमेल पते केवल एकल उपयोग के लिए हैं और आपके अलावा किसी अन्य द्वारा एक्सेस नहीं किए जा सकते।";
						$lang["seo_fast"] = "उपवास";
						$lang["seo_fast_p"] = "जितनी जल्दी हो सके ई-मेल आपके इनबॉक्स में गिर जाएंगे। इसलिए आपको इंतजार नहीं करना पड़ेगा।";
						$lang["seo_ex_time"] = "विस्तार योग्य समय";
						$lang["seo_ex_time_p"] = "यदि आप कहते हैं कि 10 मिनट पर्याप्त नहीं हैं, तो आप समय को एक घंटे तक बढ़ा सकते हैं।";

					//Footer
						$lang["about"] = "के बारे में";
						$lang["privacy"] = "एकांत";
						$lang["terms"] = "शर्तें";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> ईमेल बनाए।";

				//TOOLTIPS

						$lang["what_is_this"] = "यह क्या है?";
						$lang["copy"] = "प्रतिलिपि";
						$lang["copied"] = "कॉपी किया गया!";
						$lang["extend_time"] = "समय को बढ़ाएं";
						$lang["refresh"] = "ताज़ा करना";

				//MODALS

					//Languages
						$lang["languages"] = "बोली";
						$lang["hindu"] = "<b>हिंदू</b>";

					//About
						$lang["whats_10mails"] = "10mails.net क्या है?";
						$lang["10mails_is"] = "10mails.net एक अस्थायी ईमेल सेवा है। हमने इसे आपके काम के लिए बनाया है और एक निश्चित अवधि के अंत में हटाए गए ईमेल पतों के साथ जंक ईमेल से छुटकारा पाएं।";
						$lang["10mails_is_2"] = "हम आप के लिए बनाया है <b>" . $tma_name . "</b> पता। आपके सामने किसी ने भी इसका इस्तेमाल नहीं किया, और हम इसे आपके बाद किसी को नहीं देंगे। डिफ़ॉल्ट रूप से, हम प्रत्येक ईमेल पते को 10 मिनट देते हैं। आप चाहें तो \"<span class=\"giveme60btn\">.</span>\" बटन पर क्लिक करके 1 घंटे के लिए समय बढ़ा सकते हैं हम ईमेल के साथ स्थायी रूप से समाप्त हो चुके ईमेल पतों को हटाते हैं।";
						$lang["where_can_i_use"] = "मैं इसका उपयोग कहां कर सकता हूं?";
						$lang["you_can_use"] = "हो सकता है कि एक साइट से एक पुष्टिकरण ईमेल प्राप्त करने के लिए आप एक सदस्य हों या एक ही साइट पर कई सदस्यताएं खोलें। इस पद्धति के साथ, आपको हमेशा एक नया ईमेल पता नहीं खोलना पड़ता है और आप समय बचाते हैं।";
						$lang["got_it"] = "समझना";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>क्या आपको हमारा ऐप पसंद आया?</b> अपने दोस्तों को भी बताएं 😊";
						$lang["share_txt_nourl"] = "10%20%E0%A4%AE%E0%A4%BF%E0%A4%A8%E0%A4%9F%20%E0%A4%95%E0%A5%87%20%E0%A4%85%E0%A4%B8%E0%A5%8D%E0%A4%A5%E0%A4%BE%E0%A4%AF%E0%A5%80%20%E0%A4%88-%E0%A4%AE%E0%A5%87%E0%A4%B2%20%E0%A4%95%E0%A5%87%20%E0%A4%B8%E0%A4%BE%E0%A4%A5%20%E0%A4%8F%E0%A4%95%20%E0%A4%A8%E0%A4%AF%E0%A4%BE%20%E0%A4%88-%E0%A4%AE%E0%A5%87%E0%A4%B2%20%E0%A4%96%E0%A4%BE%E0%A4%A4%E0%A4%BE%20%E0%A4%AA%E0%A5%8D%E0%A4%B0%E0%A4%BE%E0%A4%AA%E0%A5%8D%E0%A4%A4%20%E0%A4%95%E0%A4%B0%E0%A5%87%E0%A4%82%20%E0%A4%94%E0%A4%B0%20%E0%A4%85%E0%A4%AA%E0%A4%A8%E0%A5%80%20%E0%A4%97%E0%A5%8B%E0%A4%AA%E0%A4%A8%E0%A5%80%E0%A4%AF%E0%A4%A4%E0%A4%BE%20%E0%A4%91%E0%A4%A8%E0%A4%B2%E0%A4%BE%E0%A4%87%E0%A4%A8%20%E0%A4%B0%E0%A4%96%E0%A5%87%E0%A4%82%E0%A5%A4%20%E0%A4%94%E0%A4%B0%20%E0%A4%AF%E0%A4%B9%20%E0%A4%AA%E0%A5%82%E0%A4%B0%E0%A5%80%20%E0%A4%A4%E0%A4%B0%E0%A4%B9%20%E0%A4%B8%E0%A5%87%20%E0%A4%B8%E0%A5%8D%E0%A4%B5%E0%A4%A4%E0%A4%82%E0%A4%A4%E0%A5%8D%E0%A4%B0%20%E0%A4%B9%E0%A5%88%21%0A%E0%A4%85%E0%A4%AC%20%E0%A4%87%E0%A4%B8%E0%A5%87%20%E0%A4%86%E0%A4%9C%E0%A4%AE%E0%A4%BE%E0%A4%8F%E0%A4%82";

					//Notifications
						$lang["error"] = "त्रुटि।";
						$lang["time_extended"] = "समय 1 घंटे बढ़ाया।";
						$lang["time_extended_err"] = "समय नहीं बढ़ाया गया। कृपया पुन: प्रयास करें।";

			}







	//CHINESE-SIMPLIFIED


			else if ($_COOKIE["LANG"] == "zh-cn") {

				//Head TAG
						$lang["title_tag"] = "10分钟的临时电子邮件 - 10mails.net";
						$lang["meta_description"] = "获取一个包含10分钟临时电子邮件的新电子邮件帐户，并保持您的隐私在线。";

				//MAIN

					//Container
						$lang["language"] = "中文(简体)";
						$lang["language_code"] = "Zh-Cn";

					//Body
						$lang["welcome"] = "欢迎！";
						$lang["this_is_your"] = "这是您的临时电子邮件地址。";
						$lang["signup_with_this"] = "使用此电子邮件注册至：";
						$lang["inbox"] = "收件箱";

						if ($mail_count > 1)
							$lang["total_mail"] = "您共有 <b></b> 封电子邮件。";
						else
							$lang["total_mail"] = "你 总共 有 <b></b> 封电子邮件。";

						$lang["no_email"] = "你还没有电子邮件。";

					//SEO
						$lang["seo_what_is"] = "什么是10分钟的临时电子邮件？";
						$lang["seo_p1"] = "在一段时间后自毁的一次性电子邮件地址。 您无需注册和登录即可使用此服务。 它也被称为临时邮件，10分钟邮件，一次性电子邮件，游击邮件，一次性电子邮件或假邮件。";
						$lang["seo_p2"] = "您需要提供电子邮件地址以在Internet上执行许多操作。 要访问论坛网站上的内容，下载文件或使用服务，您必须订阅该网站。 这将导致您的收件箱中充满垃圾邮件。 临时一次性电子邮件是防止垃圾邮件的最佳方式。";
						$lang["seo_safe"] = "安全";
						$lang["seo_safe_p"] = "电子邮件地址仅供一次性使用，除您以外的任何人都无法访问。";
						$lang["seo_fast"] = "快";
						$lang["seo_fast_p"] = "电子邮件将尽快落入您的收件箱。 所以你不必等待。";
						$lang["seo_ex_time"] = "可延长的时间";
						$lang["seo_ex_time_p"] = "如果你说10分钟是不够的，你可以将时间延长一个小时。";

					//Footer
						$lang["about"] = "关于";
						$lang["privacy"] = "隐私";
						$lang["terms"] = "条款";
						$lang["emails_created"] = "创建了 <b>" . $created_mail_count . "</b> 封电子邮件。";

				//TOOLTIPS

						$lang["what_is_this"] = "这是什么？";
						$lang["copy"] = "复制";
						$lang["copied"] = "复制！";
						$lang["extend_time"] = "延长时间";
						$lang["refresh"] = "刷新";

				//MODALS

					//Languages
						$lang["languages"] = "语言";
						$lang["chinese-simplified"] = "<b>中文(简体)</b>";

					//About
						$lang["whats_10mails"] = "什么是 10mails.net？";
						$lang["10mails_is"] = "10mails.net 是一个临时电子邮件服务。 我们为您完成工作创建了它，并在特定时间段结束时删除了已删除电子邮件地址的垃圾邮件。";
						$lang["10mails_is_2"] = "我们 已经创建 <b>" . $tma_name . "</b> 的 地址给你。 在你之前没有人使用它，我们不会把它交给你之后的任何人。 默认情况下，我们给每个电子邮件地址10分钟。 如果需要，可以通过单击 \"<span class=\"giveme60btn\">.</span>\" 按钮 将时间延长1小时。 我们会通过电子邮件永久删除过期的电子邮件地址。";
						$lang["where_can_i_use"] = "我 在哪里可以 使用 它？";
						$lang["you_can_use"] = "也许从您所属的网站收到确认电子邮件或在同一网站上开设多个会员资格。 使用此方法，您无需始终打开新的电子邮件地址，从而节省时间。";
						$lang["got_it"] = "了解";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>你喜欢我们的应用吗？</b> 让你的朋友也知道 😊";
						$lang["share_txt_nourl"] = "%E8%8E%B7%E5%8F%96%E4%B8%80%E4%B8%AA%E5%8C%85%E5%90%AB10%E5%88%86%E9%92%9F%E4%B8%B4%E6%97%B6%E7%94%B5%E5%AD%90%E9%82%AE%E4%BB%B6%E7%9A%84%E6%96%B0%E7%94%B5%E5%AD%90%E9%82%AE%E4%BB%B6%E5%B8%90%E6%88%B7%EF%BC%8C%E5%B9%B6%E4%BF%9D%E6%8C%81%E6%82%A8%E7%9A%84%E9%9A%90%E7%A7%81%E5%9C%A8%E7%BA%BF%E3%80%82%20%E5%AE%83%E5%AE%8C%E5%85%A8%E5%85%8D%E8%B4%B9%EF%BC%81%0A%E7%8E%B0%E5%9C%A8%E8%AF%95%E8%AF%95%E5%90%A7";

					//Notifications
						$lang["error"] = "错误。";
						$lang["time_extended"] = "时间延长1小时。";
						$lang["time_extended_err"] = "时间没有延长。 请再试一次。";

			}







	//JAPANESE


			else if ($_COOKIE["LANG"] == "ja") {

				//Head TAG
						$lang["title_tag"] = "10分の一時的な電子メール - 10mails.net";
						$lang["meta_description"] = "10分の一時的な電子メールで新しい電子メールアカウントを取得し、あなたのプライバシーをオンラインに保ちます。";

				//MAIN

					//Container
						$lang["language"] = "日本語";
						$lang["language_code"] = "Ja";

					//Body
						$lang["welcome"] = "ようこそ！";
						$lang["this_is_your"] = "これはあなたの一時的なメールアドレスです。";
						$lang["signup_with_this"] = "このメールでサインアップしてください：";
						$lang["inbox"] = "受信トレイ";

						if ($mail_count > 1)
							$lang["total_mail"] = "あなたは 合計で <b></b> 通 の電子メールを 持っています。";
						else
							$lang["total_mail"] = "あなたは 合計で <b></b> 通の 電子メールを 持っています。";

						$lang["no_email"] = "あなたはまだEメールを持っていません。";

					//SEO
						$lang["seo_what_is"] = "10分間の一時的なメールとは何ですか？";
						$lang["seo_p1"] = "一定の時間が経過すると自己破壊する使い捨ての電子メールアドレス。 このサービスを使用するために登録してログインする必要はありません。 一時メール、10分メール、使い捨てメール、ゲリラメール、使い捨てメール、または偽メールとも呼ばれます。";
						$lang["seo_p2"] = "インターネット上で多くの操作を実行するには、電子メールアドレスを提供する必要があります。 フォーラムサイトのコンテンツにアクセスしたり、ファイルをダウンロードしたり、サービスを使用したりするには、そのサイトを購読する必要があります。 これにより、受信トレイがスパムメールでいっぱいになります。 一時的な使い捨ての電子メールは、スパムメールから保護するための最良の方法です。";
						$lang["seo_safe"] = "確保します";
						$lang["seo_safe_p"] = "電子メールアドレスは1回限りの使用であり、あなた以外はアクセスできません。";
						$lang["seo_fast"] = "速いです";
						$lang["seo_fast_p"] = "電子メールはできるだけ早く受信トレイにドロップされます。 だから、待つ必要はありません。";
						$lang["seo_ex_time"] = "延長可能時間";
						$lang["seo_ex_time_p"] = "10分では足りないと言う場合は、時間を1時間延長することができます。";

					//Footer
						$lang["about"] = "約";
						$lang["privacy"] = "プライバシー";
						$lang["terms"] = "条項";
						$lang["emails_created"] = "<b>" . $created_mail_count . "</b> の電子メールが作成されました。";

				//TOOLTIPS

						$lang["what_is_this"] = "これは何ですか？";
						$lang["copy"] = "コピーする";
						$lang["copied"] = "コピーしました！";
						$lang["extend_time"] = "時間を延ばす";
						$lang["refresh"] = "更新する";

				//MODALS

					//Languages
						$lang["languages"] = "言語";
						$lang["japanese"] = "<b>日本語</b>";

					//About
						$lang["whats_10mails"] = "10mails.net とは 何 ですか？";
						$lang["10mails_is"] = "10mails.net は一時的な電子メールサービスです。 あなたの仕事を終わらせ、一定期間の終わりに削除されたEメールアドレスが付いているがらくたEメールを取り除くためにそれを作成しました。";
						$lang["10mails_is_2"] = "私たちは、 作成しました ' あなたのための <b>" . $tma_name . "</b> アドレス。 誰もあなたの前でそれを使っていません、そして私たちはあなたの後で誰にもそれを与えません。 デフォルトでは、各メールアドレスを10分にしています。 必要に応じて \"<span class=\"giveme60btn\">.</span>\" ボタンを クリックして、1時間延長することができ ます。 有効期限が切れたEメールアドレスをEメールで永久に削除します。";
						$lang["where_can_i_use"] = "どこで それを 使用する ことができ ますか？";
						$lang["you_can_use"] = "あなたがメンバーであるサイトから確認のEメールを受け取るか、または同じサイトで複数のメンバーシップを開く可能性があります。 この方法では、常に新しいEメールアドレスを開く必要はなく、時間を節約できます。";
						$lang["got_it"] = "わかる";

				//POPUPS

					//Share
						$lang["do_u_like"] = "<b>あなたは私たちのアプリが好きですか？</b> 友達にも知らせましょう 😊";
						$lang["share_txt_nourl"] = "10%E5%88%86%E3%81%AE%E4%B8%80%E6%99%82%E7%9A%84%E3%81%AA%E9%9B%BB%E5%AD%90%E3%83%A1%E3%83%BC%E3%83%AB%E3%81%A7%E6%96%B0%E3%81%97%E3%81%84%E9%9B%BB%E5%AD%90%E3%83%A1%E3%83%BC%E3%83%AB%E3%82%A2%E3%82%AB%E3%82%A6%E3%83%B3%E3%83%88%E3%82%92%E5%8F%96%E5%BE%97%E3%81%97%E3%80%81%E3%81%82%E3%81%AA%E3%81%9F%E3%81%AE%E3%83%97%E3%83%A9%E3%82%A4%E3%83%90%E3%82%B7%E3%83%BC%E3%82%92%E3%82%AA%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%B3%E3%81%AB%E4%BF%9D%E3%81%A1%E3%81%BE%E3%81%99%E3%80%82%20%E3%81%9D%E3%81%97%E3%81%A6%E3%81%9D%E3%82%8C%E3%81%AF%E5%AE%8C%E5%85%A8%E3%81%AB%E7%84%A1%E6%96%99%E3%81%A7%E3%81%99%EF%BC%81%0A%E4%BB%8A%E3%81%99%E3%81%90%E8%A9%A6%E3%81%99";

					//Notifications
						$lang["error"] = "エラー。";
						$lang["time_extended"] = "時間は1時間延長されました。";
						$lang["time_extended_err"] = "時間は伸びません。 もう一度やり直してください。";

			}







	//GENERAL AFTER

	$lang["share_txt"] = $lang["share_txt_nourl"] . "%20https%3A%2F%2F10mails.net";


?>