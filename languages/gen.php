<?php

	$lang = array();

	// LANGUAGES

	$lang["indonesian"] = "Bahasa Indonesia";
	$lang["german"] = "Deutsch";
	$lang["english"] = "English";
	$lang["spanish"] = "Español";
	$lang["french"] = "Français";
	$lang["italian"] = "Italiano";
	$lang["dutch"] = "Nederlands";
	$lang["norwegian"] = "Norsk";
	$lang["portuguese"] = "Português";
	$lang["swedish"] = "Svenska";
	$lang["turkish"] = "Türkçe";
	$lang["russian"] = "Русский";
	$lang["ukranian"] = "Українська";
	$lang["hindu"] = "हिंदू";
	$lang["chinese-simplified"] = "中文(简体)";
	$lang["japanese"] = "日本語";


	$lang["copyright"] = "<b>10mails.net</b> © 2019";


	$lang["share_txt"] = $lang["share_txt_nourl"] . "%20https%3A%2F%2F10mails.net";

?>