<div class="footer">
	<div class="footer-inner">
		<div class="how-many-mail">
			<span class="how-many-mail-inner" id="hmmi"></span>
			<span><?=$lang["emails_created"]?></span>
		</div>
		<div class="about">
			<ul class="inline">
				<li class="padr10">
					<a href="about"><?=$lang["about"]?></a>
				</li>
				<li class="pad10-10">
					<a href="privacy"><?=$lang["privacy"]?></a>
				</li>
				<li class="pad10-10">
					<a href="terms"><?=$lang["terms"]?></a>
				</li>
				<li class="pad10-10"><?=$lang["copyright"]?></li>
			</ul>
		</div>
	</div>
</div>